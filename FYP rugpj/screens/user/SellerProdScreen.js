import React, { useState,useEffect, useCallback, useReducer} from 'react';
import {View,  ScrollView,StyleSheet, Platform, Alert, KeyboardAvoidingView, ActivityIndicator} from 'react-native';


import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import {useSelector, useDispatch} from 'react-redux'
import HeaderIcon from '../shop/HeaderIcon';

import Colors from '../../constants/Colours'


import * as actionProduct from '../../store/actions/products'
// import { useEffect } from 'react';

import  Input from '../shop/Inputt';



const INPUT_UPDATE='INPUT_UPDATE';
const inputReducer = (state, action)=>{//state is our current snapshop
    if(action.type===INPUT_UPDATE){//FORM_INPUT_UPDATE is action identifier we chose to name it like that
                                        //if this action (FORM_INPUT_UPDATE) occurs we want to store input value and validate it

        const updatedValues={
            ...state.inputValues,//copyng existing state inputValues// copies old values and ....
            [action.input]: action.value//action.input yra inputIdentifier, kuris pasikeicia dynamically del siu skliaustu []  .....replaces with new vlues
        }

        const updatedIfValid={
            ...state.inputValidities,
            [action.input]: action.isValid//updates validties with new validties with the help of textChanheHandlerwhich has if function to check validities 
        }
        //updatedInputsAreValid
        let updatedInputsAreValid=true;
        for(const i in updatedIfValid){// we check every formInputValidity and if they all are valid then we overall form is valid
            updatedInputsAreValid=updatedInputsAreValid && updatedIfValid[i];//here fals overrides true thus if atlest one vilidty is not valid whole form validity is not valid
        }
        //InputsAreValid
        return {
            InputsAreValid: updatedInputsAreValid,
            inputValidities: updatedIfValid,
            inputValues: updatedValues//we are updating our inputValues with updatedValues
        };

    }
    return state;//if some other action was dispatched then we retun state
}





const SellerProdScreen=props=>{

    // const [isLoading, setIsLoading]=useState(false)//usinng this for loading thingy
    const [error, setError]=useState();

    const pId=props.navigation.getParam('productId');//getting the id of product we want to edit

    // checks if we are in edit mode and thus we populate our screen
    const userPickedProduct=useSelector(state=>
        state.products.userProducts.find(prod=>prod.id===pId)//checking if product id you are looking at is the same as product id retrieved from parameters
        );



    const dispatch=useDispatch();//creating dispatch function

// we are using useReducer from react not from redux this time
//useReducers is a hook that helps us with state management, we use it when we have connected states of morecomplex states
//and we dont want to have a lot of individual useState calls and individual states, thus we use useReducer
  
//UserReducer takes reducer function(formReducer), it also takes second argument as an initial state we want to pass in (inputValues)

//inputState changes with any keystrole(input) or change we make
const [inputState, dispatchInputState]=useReducer(inputReducer, {//formReducer should be able to change our initial state(second agument) when actions are dispatched, and action should be dispatched whenever we type in our input that should be a trigger
    //inputState is snapshot of current state and dipatchFormState is the function that changes inputState    
    inputValues: {
            // managing state, if there userPickedProduct is set basically if we are in edit mode then load in this case title and etc if not then leave empty space   
            title: userPickedProduct ? userPickedProduct.title : '',
            imageUrl:userPickedProduct ? userPickedProduct.imageUrl : '',
            description: userPickedProduct ? userPickedProduct.description : '',
            price:''
        }, 
        inputValidities:{
            //check if userPickedProduct is set and if it i set then we are editing and title initially will be prepopulated, thus the value
            // will be true otherwise it will be false
            title: userPickedProduct ? true : false,
            imageUrl: userPickedProduct ? true : false,
            description: userPickedProduct  ? true : false,
            price: userPickedProduct ? true : false
        }, 
        InputsAreValid: userPickedProduct ? true : false
    })

    //SITAS codas BUVO PRIES VALIDITY CHECK
    // managing state, if there userPickedProduct is set basically if we are in edit mode then load in this case title and etc if not then leave empty space   
    // const [title, setTitle]=useState(userPickedProduct ? userPickedProduct.title : '');
    // const [imageUrl, setImageUrl]=useState(userPickedProduct ? userPickedProduct.imageUrl : '');
    // const [price, setPrice]=useState(userPickedProduct ? userPickedProduct.price : '');
    // const [description, setDescription]=useState(userPickedProduct ? userPickedProduct.description : '');

    // const [titleIsValid, setTitleIsValid]=useState(false);


    
//Validate.js library could be installed for validation




useEffect(()=>{
    if(error){
        Alert.alert('An error occured', error, [{text:'Okay'}])
    }

}, [error])

//useCallback ensures the function isnt recreated everytime the component rerenders hus we dont get infiniteloop
    const submitHandler=useCallback(async()=>{
        if(!inputState.InputsAreValid){//checking if our whole form is valid via inputState
            Alert.alert('Bad input','Check the errors in the form', [{text:'Ok'}]);//error message with the button
            return;

        }

        setError(null);
        // setIsLoading(true)


        try{
            if(userPickedProduct){//here we checking if we editing or if we adding a product
                await dispatch(actionProduct.updateProduct(
                    pId,
                    inputState.inputValues.title, //we derive our values from inputState
                    inputState.inputValues.description, 
                    inputState.inputValues.imageUrl
                    ))
               
            }else{
                await dispatch(actionProduct.createProduct(
                    inputState.inputValues.title, 
                    inputState.inputValues.description, 
                    inputState.inputValues.imageUrl, 
                    +inputState.inputValues.price//price is covertend into number with the + sign
                    ))//+price because price is just a string which does not work
                    
            }
            props.navigation.goBack();
        }catch(err){
            setError(err.message)
        }
        
        // setIsLoading(false)//loaading sate

    },[dispatch, pId, inputState])//we are writing this empty array [] here thus the function wouoldnt be recreated, WE also add titleIsValid because if we didint add it here then function wouldnt be rebuilt when the titleIsValid validity changes, whch means we couldnt sumbit our form 

    useEffect(()=>{//executes function after a rerender cycle
        props.navigation.setParams({submit: submitHandler});//we are passing an object and adding a key- submit for submitHandler, thus now submit is a parameter that we can retrieve in the header
        //params helps us to connect header and component

    },[submitHandler]);//sumbithandler is our dependency which never changes, and now it only executes one


    //const textChangeHandler=(inputIdentifier, text)
    const inputUpdateHandler=useCallback((inputIdentifier, inputValue, inputValidity)=>{//textChangeHandler receives inputIdentifier(for itentification of which 9nput has triggered the function) and text
        //per useCallback gaunam inputIdentifier, inputValue, inputValidity duomenis is input componento
        // let isValid=false;
        // if(text.trim().length>0)//tikrina ar ne tuscias title inputas trim naudojamas anuliuot white spaces
        // {
        //     isValid=true;
        //     // setTitleIsValid(false);// before vlidity check
        // }
        // }else{
        //     // setTitleIsValid(true)// before validity check
        // }
        //setTitle(text); // before validity check
        dispatchInputState({
            type: INPUT_UPDATE,
            value: inputValue, 
            isValid: inputValidity,
            input: inputIdentifier//key for letting our reducer know which input triggered this action
        } )
    }, [dispatchInputState])

    // if(isLoading){
    //     return <View style={styles.centered}>
    //         <ActivityIndicator size='large' colo={Colors.primary}/>
    //         </View>
    // }
    return (
        <KeyboardAvoidingView 
        style={{flex: 1}}//important to have an effect on our screen
        behavior="padding"
        keyboardVerticalOffset={100}
        >
            
        <ScrollView>
            <View style={styles.formControl}>
             {/* <View style={styles.formControl}>
                <Text style={styles.label}>title</Text>
                <TextInput 
                style={styles.input} 
                value={inputState.inputValues.title} //we are using inputState to pass back values
                onChangeText={textChangeHandler.bind(this, 'title')}// kaip inputIdentifier mes paduodam 'title' kuris parodo kad keiciam title
                keyboardType='default'
                autoCapitalize='sentences'//capitaizes
                autoCorrect
                returnKeyType='next'
                /> 
                
            </View>  */}
            {/* {!formState.inputValidities.title&&<Text>error message</Text>} */}
            
            {/* litle error message */}

{/*  we are using input from input component */}
                <Input
                  id='title'
                  label='Title'
                  errorText='Please enter valid title'
                  keyboardType='default'
                  autoCapitalize='sentences'//capitaizes
                  autoCorrect
                  returnKeyType='next'
                  onInputChange={inputUpdateHandler}
                  initialValue={userPickedProduct? userPickedProduct.title:''}
                  initiallyValid={!!userPickedProduct}//if we have no userPickedProduct then we send false
                  required
                />

            

            {/* <View style={styles.formControl}>
                <Text style={styles.label}>Url image</Text>
                <TextInput style={styles.input}
                value={inputState.inputValues.imageUrl} 
                onChangeText={
                    // text=>setImageUrl(text)
                    textChangeHandler.bind(this, 'imageUrl')//paduodam imgaeUrl kaip identifier, jis sutampa su tuo ka turim useReducer'yje
                    }/>
            </View> */}

                <Input
                  id='imageUrl'
                  label='Image Url'
                  errorText='Please enter valid imageurl'
                  keyboardType='default'

                  returnKeyType='next'
                  onInputChange={inputUpdateHandler}

                  initialValue={userPickedProduct? userPickedProduct.imageUrl:''}
                  initiallyValid={!!userPickedProduct}
                  required
                />




{/* we are loadind the view below only if we are in adding mode thus if we are in edditing mode we dont see the price input*/}
            {userPickedProduct ? null :(
            // <View style={styles.formControl}>
            //     <Text style={styles.label}>Price</Text>
            //     <TextInput style={styles.input}
            //     value={inputState.inputValues.price} 
            //     onChangeText={
            //     //    text=>setPrice(text)
            //     textChangeHandler.bind(this, 'price')
            //     }
            //     keyboardType='decimal-pad'
            //     />
            // </View>
            <Input
            id='price'
            label='Price'
            errorText='Please enter valid price'
            keyboardType='decimal-pad'

            returnKeyType='next'
            onInputChange={inputUpdateHandler}

            required
            min={0.1}

          />
            )}

            {/* <View style={styles.formControl}>
                <Text style={styles.label}>Description</Text>
                <TextInput style={styles.input}
                value={inputState.inputValues.description} 
                onChangeText={
                   // text=>setDescription(text)
                   textChangeHandler.bind(this, 'description')
                    }/>
            </View> */}
                <Input
                  id='description'
                  label='Description'
                  errorText='Please enter valid description'
                  keyboardType='default'
                  autoCapitalize='sentences'//capitaizes
                  autoCorrect
                  multiline//enables tu entre multiple lines
                  numberOfLines={3}// now you can enter 3 lines
                  onInputChange={inputUpdateHandler}

                  initialValue={userPickedProduct? userPickedProduct.description:''}
                  initiallyValid={!!userPickedProduct}
                  required
                  minLength={5}
                />
            </View>

        </ScrollView>
        </KeyboardAvoidingView>
        
    )

    

}

SellerProdScreen.navigationOptions=navData=>{//makeing dynamic header
    const submitFn=navData.navigation.getParam('submit');//here we retrieving the submit parameter from props.navigation.setParams({submit: submitHandler});
    return{
        headerTitle: navData.navigation.getParam('productId')//checking if we get productId (it checks wheter we are editing if we get id or adding if we dont get id) 
        ? 'Edit Product' 
        : 'Add Product',
        headerRight:(
        <HeaderButtons HeaderButtonComponent={HeaderIcon}>
        <Item 
        title="Save" iconName={Platform.OS==='android'?"md-checkmark":'ios-checkmark' }
        onPress={submitFn}
        />
        </HeaderButtons>
        )

    }
}


const styles=StyleSheet.create({
    form:{
        margin: 20
    },
    formControl:{
        width:'100%'

    },
    label: {
        marginVertical: 8
    },
    input:{
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1

    },
    centered:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    }

})

export default SellerProdScreen;