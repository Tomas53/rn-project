import React from 'react';
import { FlatList, Button, Alert, View, Text } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';//to get data from reducer

import HeaderIcon from '../shop/HeaderIcon';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import ProductComponent from "../shop/ProductComponent"
import Colours from '../../constants/Colours'


import * as productActions from '../../store/actions/products'

const UserProductsScreen=props=>{

    const userProducts=useSelector(state=>state.products.userProducts)//ziuri i root reduceri App.js is ten eina i product reducer folderuje ir accessina userProducts
    const dispatch=useDispatch();

    const updateProductHandler=(id)=>{
        props.navigation.navigate('EditProduct', {productId: id})//here we are pointing to the navigation to EditProduct identifier we created(named it in that way)
            //we are forwarding productId in code up
    }


    const deleteProdHandler=(id)=>{//passing id here
        Alert.alert('Are you sure?', 'do you want to delete it',
        [
            {text:'No', style: 'default'},//button nr 1
            {text:'yes', style:'destructive', onPress:()=>{//button nr 2
                    
                    dispatch(productActions.deleteProduct(id));//we are choosing deleteProduct via our actions and then we passing productId
                }
            }
        ])
    }


    if(userProducts.length===0){
        return <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
            <Text>You have no products created</Text>
        </View>
    }

        return(<FlatList 
        data={userProducts} 
        keyExtractor={item=>item.id}
        renderItem={prodData=><ProductComponent image={prodData.item.imageUrl}//prodData.item points to our product model in model folder
        title={prodData.item.title} 
        price={prodData.item.price}
        onSelect={()=>{

            updateProductHandler(prodData.item.id)//we are forwarding id to our function

        }} 
      
        >

            <Button 
            color={Colours.price}
            title="Edit" 
            onPress={()=>{
                updateProductHandler(prodData.item.id)//we are forwarding id to our function
            }}
  />
            <Button 
            color={Colours.price}
            title="Delete" 
            onPress={()=>{deleteProdHandler(prodData.item.id)//we forwarding the id from prodData.item.id to deleteProdHandler
            //     ()=>{
            //     //dispatching our action
            //     disp(productActions.deleteProduct(prodData.item.id));//we are choosing deleteProduct via our actions and then we passing productId
            // }
        }}
            />
        </ProductComponent>
        
    }
    />)
}

UserProductsScreen.navigationOptions=navData=>{
    return{headerTitle:'Your products',
    headerLeft: (
        <HeaderButtons HeaderButtonComponent={HeaderIcon}>
        <Item title="Menu" iconName="md-menu" 
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.toggleDrawer();
        }}/>
        </HeaderButtons>
        ),

    headerRight:(
        <HeaderButtons HeaderButtonComponent={HeaderIcon}>
        <Item title="Add" iconName="md-create" 
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.navigate('EditProduct');//we are pointing to editproduct however we dont pass any parameters because we are creating a new product
        }}/>
        </HeaderButtons>
    )
}}//pridedam headeri sitam screenui
export default UserProductsScreen;