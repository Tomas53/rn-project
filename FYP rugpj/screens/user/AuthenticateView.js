import React, {useState, useEffect,useReducer, useCallback} from 'react';


import Input from '../shop/Inputt';
import {ScrollView,Alert, View,Button, KeyboardAvoidingView, StyleSheet, ActivityIndicator} from 'react-native'

import Colours, { Colors } from '../../constants/Colours';

import { useDispatch } from 'react-redux'// lets us dispatch actions

import * as authActions from '../../store/actions/auth'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from '../shop/HeaderIcon';

const INPUT_UPDATE='INPUT_UPDATE';

const inputReducer = (state, action)=>{//state is our current snapshop
    if(action.type===INPUT_UPDATE){//FORM_INPUT_UPDATE is action identifier we chose to name it like that
                                        //if this action (FORM_INPUT_UPDATE) occurs we want to store input value and validate it

        const updatedValues={
            ...state.inputValues,//copyng existing state inputValues// copies old values and ....
            [action.input]: action.value//action.input yra inputIdentifier, kuris pasikeicia dynamically del siu skliaustu []  .....replaces with new vlues
        }

        const updatedIfValid={
            ...state.inputValidities,
            [action.input]: action.isValid//updates validties with new validties with the help of textChanheHandlerwhich has if function to check validities 
        }
        let updatedInputsAreValid=true;
        for(const i in updatedIfValid){// we check every formInputValidity and if they all are valid then we overall form is valid
            updatedInputsAreValid=updatedInputsAreValid && updatedIfValid[i];//here fals overrides true thus if atlest one vilidty is not valid whole form validity is not valid
        }
        return {
            InputsAreValid: updatedInputsAreValid,
            inputValidities: updatedIfValid,
            inputValues: updatedValues//we are updating our inputValues with updatedValues
        };

    }
    return state;//if some other action was dispatched then we retun state
}

const AuthenticateView =props=>{
    //const [isLoading, setIsLoading]=useState(false);
    // const [isSignup, setIsSignup]= useState(false)// tells us if we are are in signup mode or in login mode
    const [error, setError] = useState();
    const disp= useDispatch()// getting access to dispatch function

    const [inputState, dispatchInputState]=useReducer(inputReducer, {//dispatchFormState is triggered when our input changes (onInputChange)
        //formState is snapshot of current state and dipatchFormState is the function that changes formState    
        inputValues: {
                email:'',// innitialy is empty
                password:''// innitialy is empty
            }, 
            inputValidities:{
                email: false,//initially is not valid
                password: false//initially is not valid
            }, 
            InputsAreValid:  false//initially is not valid
        })

        useEffect(()=>{
            if(error){
                Alert.alert('There is an error', error,[{text: 'Ok'}])
            }
        },[error])
    

    const loginHandler=async ()=>{// we add async because our action creator returns a promise
        let action;// our action initialy is undefined
        action=authActions.login(
            inputState.inputValues.email,
            inputState.inputValues.password)
        // if(isSignup){//if true then we 

        // action= authActions.signup(//autchActions action
        //     formState.inputValues.email,//to sign up we need these values
        //     formState.inputValues.password
        //     )
        // }else{
        // action=authActions.login(
        //     formState.inputValues.email,
        //     formState.inputValues.password)
        // }
        setError(null)
        // setIsLoading(true)//it is used for loading spiner thus before we start this process (before we send a request)
        try{
            await disp(action)// we dispaching our action here, its either login or sign up according to the if statement, we add await to await for the result of dispatch which uses the promises above
            props.navigation.navigate('Shop')// jei prisilogini tai nukelia tave i products screena
        }catch(err){
            setError(err.message)
            // setIsLoading(false)  // once we are done with the request

        }

    }


    const inputUpdateHandler= useCallback(
        (inputIdentifier, inputValue, inputValidity)=>{//useCallback prevents from rerendering when it shouldnt
 
        dispatchInputState({
            type: INPUT_UPDATE,
            value: inputValue, 
            isValid: inputValidity,
            input: inputIdentifier//key for letting our reducer know which input triggered this action
        })
        }, 
        [dispatchInputState]
        
        );
        
//test@gmail.com
//testtest
    return <KeyboardAvoidingView 
    behavior="padding"
    keyboardVerticalOffset={40}
    style={styles.window}
    >
        <ScrollView>
            <View>
            <Input
            id="email"
            label="E-mail"
            keyboardType="email-address"
            required
            email
            autoCapitalize="none"
            errorText="Please enter a valid email"
            onInputChange={inputUpdateHandler}//thiggers the function, thus we are now storing our input values in FormState (dispatchFormState) which we handle with useReducer
            initialValue=""
            />
            </View>
            <View>
            <Input
            id="password"
            label="Password"
            keyboardType="default"
            secureTextEntry//secureTextEntry is not our custom input feature but our component has text input import thus it support this feature
            required
            minLength={5}
            autoCapitalize="none"
            errorText="Please enter a valid password"
            onInputChange={inputUpdateHandler}
            initialValue=""
            />
            </View>
           
            {/* triggers Sign Up function*/}
            {/* checks if we are in signup or in login mode and changes the button title accordingly */}
           {/* {isLoading?(<ActivityIndicator size='small' color={Colours.primary}/>):(  */}
           <Button title={'Login'} color='#17c4bd' onPress={loginHandler}/>
           {/* )} */}
            {/* changes the state from tro to false or vise versa */}
            {/* <Button title={`Switch to ${isSignup?'Login':'Sign up'}`} color={Colours.accent} onPress={()=>{ setIsSignup(prevState=>!prevState)}}/> */}

        </ScrollView>
    </KeyboardAvoidingView>
};




AuthenticateView.navigationOptions=navData=>{
    return{
        headerTitle: 'Login',
      
        
        headerRight: (
        <HeaderButtons HeaderButtonComponent={HeaderIcon}>
            <Item title="SignUp"  
            onPress={()=>{
                // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
                navData.navigation.navigate('Signup');
            }}/>
        </HeaderButtons>
        )
      }
};


const styles =StyleSheet.create({
    window:{
        justifyContent:'center',
        flex:1,
       
        alignItems:'center'
    },
    // authContainer:{
    //     width: '90%',
    //     maxWidth: 415,
       
    //     maxHeight:415,
    //     padding:25
    // }

});

export default AuthenticateView;