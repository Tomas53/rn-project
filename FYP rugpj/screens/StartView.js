// we showing this screen when our app is booting up and user is being authenticated
import React, {useEffect} from 'react';
import {
    View,
    ActivityIndicator,
    StyleSheet,
    AsyncStorage

} from 'react-native'

import Colours from '../constants/Colours'
import {useDispatch} from 'react-redux'
import * as authActions from '../store/actions/auth'
const StartView = props => {


    const disp = useDispatch();
    useEffect(()=>{
        const tryLogin=async ()=>{// creating tryLogin inner function just to use async await
            const userData= await AsyncStorage.getItem('userData');// we use userData because we named our asyncStorage like this in actions file
            if(!userData){// triggers when we are not logged in
                props.navigation.navigate('Auth')// brings us to authentication scren
                return;
            }
            const packedInfo=JSON.parse(userData)//turns data into js object
            const {token, userId, expiryDate}=packedInfo//retrieving token, userId, expiryDate from AsyncStorage

            const expTime= new Date(expiryDate);

            if(expTime<=new Date()||!token||!userId){// whecking if token is valid, if we can fnd a token or if we can find a user
                props.navigation.navigate('Auth')// brings us to authentication scren

                return;
            }

            props.navigation.navigate('Shop')// if everyting is ok we navigate user to shop screen
            disp(authActions.authenticate(userId,token))// we log user
        }
        tryLogin();

    }, [disp])




    return (
    <View style={styles.screen}>
        {/* <ActivityIndicator size='large' color={Colours.primary}/> */}
    </View>
    )
}

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
export default StartView;