import {View, Text, TextInput, StyleSheet} from 'react-native';
import React, {useReducer, useEffect} from 'react';


const INPUT_UPDATE='INPUT_UPDATE';
const INPUT_FOCUS='INPUT_FOCUS';

const reducerIn=(state, action)=>{
    switch(action.type){//checks action type
        case INPUT_FOCUS:
            return{
                ...state,
                touched:true
            }
        case INPUT_UPDATE:
            return{
                ...state,
                value: action.value,//updating value
                isValid: action.isValid//checks if specific input is valid and not entire form
            }
       
        
            default:
                return state;
    }
};

const Input=props=>{
    //returns us inputState snapshot and dispatch function
    const [inpState, dispatch]=useReducer(reducerIn, {
        value: props.initialValue ? props.initialValue: '',//check if initil value is set basically if we in edit mode
        isValid: props.initiallyValid,//checks if props are valid
        touched: false//checks if user typed anything into form
    })

    const {onInputChange, id}=props;
    useEffect(()=>{
        if(inpState.touched){//we only send this info if touched is true
            onInputChange(id,inpState.value, inpState.isValid);//parent function if triggered receives these arguments

        }
    }, [inpState, onInputChange, id]);//useEffect runs when inputState, onInputChange, id changes
    
    const touchUpdateHandler=()=>{//we update touch when we loose focus(when user is done typing in certain input area)
        dispatch({type:INPUT_FOCUS})

    }
    const inputUpdateHandler=text=>{
        //check if input valid or not

        const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // email check regex
        let isValid = true;
        
        if (props.email && !regexEmail.test(text.toLowerCase())) {
        isValid = false;
        }
        if (props.required && text.trim().length === 0) {
            isValid = false;
            }
        
        if (props.max != null && +text > props.max) {
        isValid = false;
        }
        //checks if number is too big or too small
        if (props.min != null && +text < props.min) {
            isValid = false;
            }
        if (props.minLength != null && text.length < props.minLength) {
        isValid = false;
        }
        dispatch({type: INPUT_UPDATE, value:text, isValid:isValid})//disatches action

    }


 
    return <View style={styles.formStyle}>
                <Text style={styles.label}>{props.label}</Text>{/* here text is dynamic because of props.label */}
                {/* validation for form happens here and not in the parent component */}
                <TextInput 
                {...props}
                style={styles.input} 
                value={inpState.value} //we are using inputState as our internal state
                onChangeText={inputUpdateHandler}//
                onBlur={touchUpdateHandler}//activates when user types in this specific input
                />
                {!inpState.isValid&& inpState.touched &&(
                    <View style={styles.error}>
                <Text style={styles.text}>{props.errorText}</Text>{/*we have dynamic eroor message*/}
                </View>
                )}
            </View>
            
            {/* litle error message */}

    

}

const styles=StyleSheet.create({
    formStyle:{
        width:'100%'

    },
    label: {
        marginVertical: 8
    },
    input:{
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1

    },
    error: {
        marginVertical: 5
    },
    text:{
        color: 'red',
        fontSize: 12
    }
})
export default Input;