
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from "react-native";
import {Ionicons} from "@expo/vector-icons"

const X=props=>{
    return( 

            <TouchableOpacity onPress={props.onRemove} style={styles.deleteButton}>
                <Ionicons 
                  name="md-close"
                  size={21}
                  color="pink"
                />
            </TouchableOpacity>
   
    )
};

const styles=StyleSheet.create({
    cartProduct:{
        padding: 9,
        backgroundColor: '#FEFBDA',
        flexDirection: 'row',
        justifyContent: "space-between",
        marginHorizontal: 5
    },
    productInfo: {
        flexDirection: "column",
        alignItems: "center"
    },
    quantity:{
        color: 'black',
        fontSize: 14
        
    },
    text: {
        fontSize: 14
    },
    
    deleteButton: {
        marginLeft: 17
    }


});

export default X;