

import React from 'react';

//import {Ionicons} from "@expo/vector-icons"
import {View, Text, StyleSheet, TouchableOpacity, Platform} from "react-native";
const CartProduct=props=>{
    return( <View style={styles.cartProduct}>
        <View style={styles.productInfo2}>
             
             </View>

             
        <View style={styles.productInfo}>
        <Text styles={styles.text}> {props.title}</Text>
        <Text styles={styles.quant}>Quantity {props.quantity}</Text>
            <Text styles={styles.text}>£{props.amount.toFixed(2)}</Text>

        </View>
        
        {props.children}
       
    

    </View>
    )
};

const styles=StyleSheet.create({
    productInfo: {
        flexDirection: "column",
        alignItems: "center"
    },
    productInfo2: {
        flexDirection: "column",
        alignItems: "flex-end"
    },
    text: {
        fontSize: 14
    },
    cartProduct:{
        
        flexDirection: 'row',
        justifyContent: "space-between",
        padding: 9,
        backgroundColor: '#FEFBDA',
        marginHorizontal: 5
    },
   
    quant:{
        color: 'black',
        fontSize: 13
        
    },

    
    deleteButton: {
        marginLeft: 17
    }


});

export default CartProduct;