import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

import CartProduct from './CartProduct';

const OrderProd=props=>{
    // const [showDetails, setShowDetails]=useState(false);//we dont show details initially

    return <View style={styles.order}> 
        <View style={styles.dateStyle}>
            <Text style={styles.sum}>£{props.amount.toFixed(2)}</Text>
            <Text style={styles.dateFont}>{props.date}</Text>
        </View>
        {/* <Button 
        title={ showDetails ? "Hide details":"Show Details" }//changes titile according to showDetails value
        onPress={()=>{
            setShowDetails(prevState=>!prevState)//reverts the state
        }}/> */}
        
        { 
        <View >
              {props.items.map(cartP=>
              
              <CartProduct //if showDetails is true then we show this view
                key={cartP.productId}//map requires key thus we use unique ids of products
                quantity={cartP.quantity}//sugeneruoja CartItem detailsus
                amount={cartP.sum}
                title={cartP.productTitle}
                
                />
                )
                }
            </View>}
    </View>

}

const styles=StyleSheet.create({
    order:{
        
        shadowOffset:{width:0, height:2},
        shadowRadius: 10,
        elevation:9,
        borderRadius:11,
        shadowColor: 'grey',
        shadowOpacity: 0.30,
        backgroundColor: 'white',
        margin: 21,
        padding: 12,
        alignItems: 'center',
        borderBottomWidth:12,
        borderColor:'#F5EF9D'


    },
    dateStyle:{
        flexDirection: 'row',
        marginBottom:18,
        borderColor:'#F5EF9D',
        width: '100%',
        borderRightWidth:5,
        borderBottomWidth:5,
        justifyContent: 'space-between',
        alignItems: 'center',
 
        borderLeftWidth:5,
        backgroundColor:"#FEFBDA"
    },
    sum:{
        
        fontSize: 17
    },
    dateFont:{
        fontSize: 17

    }

});

export default OrderProd;