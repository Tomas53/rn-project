
import React from 'react';
import {View,Button,Text,ScrollView,StyleSheet,Image} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import Colours from '../../constants/Colours';
import * as actionsForCart from '../../store/actions/cart'

const ProductInfoScreen=props=>{
   
    const idProd = props.navigation.getParam('productId');
    const productChosen = useSelector(state=>
        
        state.products.availableProducts.find(prod=>prod.id===idProd));
        const disp=useDispatch();
    
        return (
        <ScrollView>
            <Image style={styles.img} source={{uri:productChosen.imageUrl}}/>
            <View style={styles.button}>
            <Button color='#17c4bd' title='Add to cart' onPress={()=>{
                disp(actionsForCart.addToCart(productChosen))
            }}/>
            </View>
            <Text style={styles.price}>£{productChosen.price}</Text>
            <Text style={styles.desc}>{productChosen.description}</Text>
        </ScrollView>
    )

    

};
ProductInfoScreen.navigationOptions=navigationData=>{
    return{

        // getParam("productTitle") gaunam is productOverviewScreen ka nurodem kaip parametra
        headerTitle: navigationData.navigation.getParam('productTitle')
    }
}



const styles=StyleSheet.create({

    // act:{
    //     marginVertical:10,
    //     alignItems:'center',
        

    // },
    img:{
        width: '100%',
        height:300
    },
    button:{
        width: 100,
        height: 20,
        alignSelf: 'center',
        marginBottom:20

    },
    price:{
        fontSize:20,
        color:'#888',
        textAlign:'center',
        marginVertical:20


    },
    desc:{
        fontSize:15,
        textAlign:'center',
        marginHorizontal: 15
    }

});

export default ProductInfoScreen;