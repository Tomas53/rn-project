import React, { Component } from 'react';
import { View, Button, Linking, StyleSheet, Text } from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';

export default class SupportScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text>FAQ:</Text>
        <Text>Q: How can I order?</Text>
        <Text>A: You can order easily using our mobile application. Choose the products you would like to buy, then add them to the cart and click on "Order now" button</Text>
        <Text>Q: How could I ge arefund?</Text>
        <Text>A: Please sen us an email providing the details about your purchase to get the refund. You can click the button below to compose an email</Text>
        
        <Text>If you have more questions please contact us by clicking on the button below</Text>
        <Button
        title="Compose an email"
         onPress={()=>Linking.openURL('mailto:support@shop.co.uk?subject=SendMail&body=Description')}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

});

SupportScreen.navigationOptions=navData=>{
    return{
    headerTitle: 'Help desk',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    // headerRight: (
    // <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    //     <Item title="Cart" iconName="md-cart" 
    //     onPress={()=>{
    //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
    //         navData.navigation.navigate('Cart');
    //     }}/>
    // </HeaderButtons>
    // )
  }
};

