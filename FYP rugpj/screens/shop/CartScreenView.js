import React from 'react';
// using useSelector to get data from our store
import {useSelector, useDispatch} from 'react-redux';
import {View, Text, FlatList, Button, StyleSheet,TouchableOpacity} from 'react-native'

// import React from 'react';
// import { View, Text, FlatList, Button, StyleSheet } from 'react-native';
// import { useSelector } from 'react-redux';
import CartProduct from './CartProduct'
import * as actionsOrders from '../../store/actions/orders'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';
import * as actionsCart from '../../store/actions/cart'
import X from './X'
import { Ionicons } from '@expo/vector-icons';
const CartScreenView=props=>{
    const cartQuantity = useSelector(state => state.cart.totalAmount)
   

    const cartProducts=useSelector(state=>{
        const cartProdArray=[];
        for (const i in state.cart.items){
            cartProdArray.push({
                productId: i,
                productTitle: state.cart.items[i].productTitle,
                productPrice: state.cart.items[i].productPrice,
                quantity: state.cart.items[i].quantity,
                sum: state.cart.items[i].sum
            });
            
        }//prdedam sortinima pagal id kad orderiai nesikeistu vietomis
        return cartProdArray
        //.sort((x, y)=> x.productId>y.productId? 1 : -1 );
        
    });

    const disp=useDispatch();
return (<View style={styles.screen}>

    <FlatList 
    data={cartProducts} 
    // keyExtractor lets react know where our key could be found
    // we choose productId as our key
    keyExtractor={data=> data.productId} 
    renderItem={data=>

    <CartProduct 
    quantity={data.item.quantity} 
    title={data.item.productTitle} 
    amount={data.item.sum}
    deletable//sets deletable to true thus we can see trach icon on our cart next to orders
   

    >
 
      <X
          onRemove={()=>{
            disp(actionsCart.removeFromCart(data.item.productId))
        }}
      />

      
      

    
    </CartProduct>
    
    }/>
        <View>
        {/* below we are fixing the -0 problem with Math.round */}
            <Text style={styles.screen}
            >Total cost:{' '}
            
            </Text>
            <Text>£{Math.round(cartQuantity.toFixed(2)*100) / 100}</Text>
            <Button 
            title ="Order" 
            disabled={cartProducts.length===0}
            onPress={()=>{
                disp(actionsOrders.addOrder(cartProducts, cartQuantity))//passing details to action
            }}/>
    </View>
</View>
)
};

CartScreenView.navigationOptions=navData=>{
    return{
    headerTitle: 'Cart',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    // headerRight: (
    // <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    //     <Item title="Cart" iconName="md-cart" 
    //     onPress={()=>{
    //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
    //         navData.navigation.navigate('Cart');
    //     }}/>
    // </HeaderButtons>
    // )
  }
  };
// CartScreenView.navigationOptions={
//     headerTitle: 'Your Cart'
// }


const styles=StyleSheet.create({
    screen:{
        alignItems: 'center',
        
    }
});


export default CartScreenView;

