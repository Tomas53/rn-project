import React, {useState} from 'react';
import { View, Button, Text, ActivityIndicator,Dimensions, Alert
, StyleSheet} from 'react-native';
import Colours from '../../constants/Colours'

import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';

import * as Location from 'expo-location'
import * as Permissions from 'expo-permissions'
import MapView ,{ Polyline, ProviderPropType }from 'react-native-maps';
import { Marker } from "react-native-maps";
const INITIAL_REGION = {
    latitude: 52.5,
    longitude: 19.2,
    latitudeDelta: 8.5,
    longitudeDelta: 8.5
  };

const LocationMap=props=>{
    const [pickedLocation, setPickedLocation]=useState({
        coord:{
        latitude: 54.5260,
        longitude: 15.2551
    }});
    const [meters, setMeters]=useState();
    const [mapRegion, setMapRegion] = useState({
        region: {
          latitude: 54.5260,
          longitude: 15.2551,
          latitudeDelta: 20,
          longitudeDelta: 20
        }
      });
       const COORDINATES = [
            { latitude: 54.5260, longitude: 15.2551 },
            { latitude: 54.5260, longitude: 15.2551 },
    ];
      const [coordinates, setCoordinates]=useState({COORDINATES})
    const verifyPermissions=async()=>{
        const result=await Permissions.askAsync(Permissions.LOCATION);
        if(result.status!=='granted'){
            Alert.alert(
                'Bad permissions',
                'Please grant permissions for location',
                [{text:'OK'}]
            );
            return false;
        }
        return true;
    }


    const getLocationHandler=async()=>{
        const hasPermission= await verifyPermissions();
        if(!hasPermission){
            return;
        }
        try{
        const location=await Location.getCurrentPositionAsync({timeout: 5555});
        const lat1=51.507351;
        const lon1=-0.127758;
        const lat2=location.coords.latitude;
        const lon2=location.coords.longitude;
        const R = 6371e3; // metres
        const φ1 = lat1 * Math.PI/180; // φ, λ in radians
        const φ2 = lat2 * Math.PI/180;
        const Δφ = (lat2-lat1) * Math.PI/180;
        const Δλ = (lon2-lon1) * Math.PI/180;
    
        const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
          Math.cos(φ1) * Math.cos(φ2) *
          Math.sin(Δλ/2) * Math.sin(Δλ/2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    
    const d = R * c/1000/100; 

        const latS1=37.773972;
        const lonS1=-122.431297;
        const latS2=location.coords.latitude;
        const lonS2=location.coords.longitude;
        const RS = 6371e3; // metres
        const φ1S = latS1 * Math.PI/180; // φ, λ in radians
        const φ2S = latS2 * Math.PI/180;
        const ΔφS = (latS2-latS1) * Math.PI/180;
        const ΔλS = (lonS2-lonS1) * Math.PI/180;
    
        const aS = Math.sin(ΔφS/2) * Math.sin(ΔφS/2) +
          Math.cos(φ1S) * Math.cos(φ2S) *
          Math.sin(ΔλS/2) * Math.sin(ΔλS/2);
        const cS = 2 * Math.atan2(Math.sqrt(aS), Math.sqrt(1-aS));
    
    const dS = RS * cS/1000/100; // in metres
    console.log(dS)

    if(d<dS){
        setMeters(d.toFixed(2))
    }else{
        setMeters(dS.toFixed(2))
    }
    
    // setCoordinates({
    //     COORDINATES:{
    //          latitude: location.coords.latitude, longitude:  location.coords.longitude ,
    //          latitude: 37.7896386, longitude: -122.421646 ,
    //     }
       

         
    
    // })
    setMapRegion({
        region: {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
          latitudeDelta: 0.4,
          longitudeDelta: 0.4
        }})
    
        console.log(location)
        console.log(d)
        setPickedLocation({
            lat: location.coords.latitude,
            lng: location.coords.longitude
        });
        
       // la=location.coords.latitude;
      //  lg=location.coords.longitude;

//console.log(d)
    }catch{
            Alert.alert('nolocation fetched',
            [{text:'OK'}])
        }
    };
    const la=51.8;
    const lg=20;

    const COLORS = [
          '#7F0000',
          '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
          '#B24112',
          '#E5845C',
          '#238C23',
          '#7F0000',
        ];
    return (
        <View style={styles.locationStyle}>
            <View styles={styles.mapPreview}>
    <Text >Approximate delivery day number: {meters} </Text>
            </View>
            <View style={styles.button}>
            <Button title="Get location" color={'#17c4bd'}  onPress={getLocationHandler}/>

            </View>
            <MapView initialRegion={mapRegion.region}
      region={mapRegion.region} style={styles.mapStyle} showsUserLocation={true}>
 
    <Marker coordinate={{ latitude: 51.507351, longitude:-0.127758 }} />
    <Marker coordinate={{ latitude: 37.773972, longitude:-122.431297 }} />

   
    {/* <Polyline
          coordinates={[
              { latitude: 51.507351, longitude:-0.127758 },
          { latitude:pickedLocation.coord.latitude, longitude: pickedLocation.coord.longitude }]}
          strokeColor="#010"
          strokeColors={COLORS}
          strokeWidth={6}
        /> */}
  </MapView>
        </View>
    )
}

const styles=StyleSheet.create({
    locationStyle:{
        marginBottom:14,

    },
    mapPreview:{
        marginBottom:21,
        width:'100%',
        height:145,
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
      mapStyle: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
      },
      button:{
        width: 200,
        height: 20,
        alignSelf: 'center',
        marginBottom: 10

    },
});
LocationMap.navigationOptions=navData=>{
    return{
    headerTitle: 'Wish list',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    // headerRight: (
    // <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    //     <Item title="Cart" iconName="md-cart" 
    //     onPress={()=>{
    //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
    //         navData.navigation.navigate('Cart');
    //     }}/>
    // </HeaderButtons>
    // )
}
};
// GradientPolylines.propTypes = {
//   provider: ProviderPropType,
// };

export default LocationMap;
// import React from 'react';
// import { StyleSheet, Dimensions } from 'react-native';

// import MapView, { Polyline, ProviderPropType } from 'react-native-maps';

// const { width, height } = Dimensions.get('window');

// const ASPECT_RATIO = width / height;
// const LATITUDE = 37.78825;
// const LONGITUDE = -122.4324;
// const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// const COORDINATES = [
//   { latitude: 37.8025259, longitude: -122.4351431 },
//   { latitude: 37.7896386, longitude: -122.421646 },
// //   { latitude: 37.7665248, longitude: -122.4161628 },
// //   { latitude: 37.7734153, longitude: -122.4577787 },
// //   { latitude: 37.7948605, longitude: -122.4596065 },
// //   { latitude: 37.8025259, longitude: -122.4351431 },
// ];

// const COLORS = [
//   '#7F0000',
//   '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
//   '#B24112',
//   '#E5845C',
//   '#238C23',
//   '#7F0000',
// ];

// class GradientPolylines extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       region: {
//         latitude: LATITUDE,
//         longitude: LONGITUDE,
//         latitudeDelta: LATITUDE_DELTA,
//         longitudeDelta: LONGITUDE_DELTA,
//       },
//     };
//   }

//   render() {
//     return (
//       <MapView
//         provider={this.props.provider}
//         style={styles.container}
//         initialRegion={this.state.region}
//       >
//         <Polyline
//           coordinates={COORDINATES}
//           strokeColor="#000"
//           strokeColors={COLORS}
//           strokeWidth={6}
//         />
//       </MapView>
//     );
//   }
// }

// GradientPolylines.propTypes = {
//   provider: ProviderPropType,
// };

// const styles = StyleSheet.create({
//   container: {
//     ...StyleSheet.absoluteFillObject,
//   },
// });

// export default GradientPolylines;