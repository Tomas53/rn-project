import React , {useState, useEffect, useCallback}from 'react';
import {FlatList,Text,Button, ActivityIndicator,Platform, View, StyleSheet} from 'react-native';
// allows us to tap into redux store and get out our products
// from there
import {useSelector, useDispatch} from 'react-redux';

import ProductComponent from './ProductComponent';
import Colours from '../../constants/Colours'
import * as cartActions from '../../store/actions/cart'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';
import * as productsActions from '../../store/actions/wishList'
import  Colors from '../../constants/Colours';


const WishListView=props=>{
    // const [isLoading, setIsLoading]=useState(false);//using this state for loading

    const [error, setError]=useState();//state for errors

    const [isRefreshing, setIsRefreshing]=useState(false);

    // sueselector takes a function which automatically receives
    // the state (redux state) as an input and which then returns
    // whichever data we wan to use

    // state.products refering to name provided in App.js (rootreducer) 
   
   
    const products =useSelector(state=>state.wishList.wishListProducts);
   
   
    // we are getting access to useDispatch function
    const dispatch=useDispatch();
//useCallBack helps us avoid infinite error
    const loadProducts = useCallback( async () =>{
        setError(null);//setting error to null before reloading products
        // setIsLoading(true);
        setIsRefreshing(true)
        try{

        
        await dispatch(productsActions.fetchProducts());//trggers http request
    } catch(err){// we get the err from product actions because we rethrow it in catch section
        setError(err.message)//error is set to the message

    }
    setIsRefreshing(false)
       // setIsLoading(false)
    },[dispatch,  setError]);


    //onl works when the component has rendered the first time, we need useEffect code below
    useEffect(()=>{
        const willFocusSub=props.navigation.addListener('willFocus', loadProducts);//didFocus works after a page was fully focused, willFocus will work when transition begins, willBlur work when we about to leave it, didBlur work once you are done
//we can also return in useEffect, which is a clean up function
// which runs whenever this effect is about to be rerun or when this components is destroyed
//it allows us to lean up this listener
        return()=>{
            willFocusSub.remove();//it gets rid of subscription when willFocusSub components is unmounted
            //or whenever it reruns  when loadProducts rebuilds which shouldnt happen

        }
    },[loadProducts]);
//test2@gmail.com testtest
    useEffect(()=>{
        // setIsLoading(true)
        loadProducts().then(()=>{
            // setIsLoading(false)
        });
    },[dispatch, loadProducts]);//it will only rerun when this components is loaded basicalyy, when we got to products page
    
    const selectItemHandler=(id, title)=>{
        // props.navigation.navigate('ProductDetail' nurodom i savo key aprasyta navigatione
        props.navigation.navigate('ProductDetail',
        // perduodam parametrus i producdetailsscreen componenta
        {
        productId: id,
        productTitle: title})

    };

    if(error){//jei true td sugeneruoja mygtuka useriui pareloadint pagea
        return ( <View style={styles.centered}>
           
        <Text>Error occured</Text>
        <Button title='reload' onPress={loadProducts} color={'#17c4bd'}></Button>

        </View>
)
    }

    // if(isLoading){//tikrina ar true jei taip tai loadina coda below
    //     return ( <View style={styles.centered}>
    //                     {/* loading zenkliukas */}
    //         <ActivityIndicator size='large' color={Colors.primary}/>

    //     </View>
    //     )
    // }
    if(products.length===0){//tikrina ar yra produktu ir ar nebeloadina, nes jei nera tada meta sita teksta
        return ( <View style={styles.centered}>
        <Text>No products found</Text>
        </View>
)
    }
        
    
    

    return <FlatList 
    onRefresh={loadProducts}//galima drag down to refresh padaryti, mes specifyinam kad patrigerintu load products
    refreshing={isRefreshing}//ziuri ar loading ar ne loading
    data={products} 
    keyExtractor={item=>item.id}
    // itemData.item yra grynai is flatlisto, imageUrl yra is musu apibrezto modelio productui atvaizduoti
    renderItem={itemData=>
    <ProductComponent 
    image={itemData.item.imageUrl}
    title={itemData.item.title}
    price={itemData.item.price}
    // onSelect={()=>{
    //             // // props.navigation.navigate('ProductDetail' nurodom i savo key aprasyta navigatione
    //             // props.navigation.navigate('ProductDetail',
    //             // // perduodam parametrus i producdetailsscreen componenta
    //             // {productId: itemData.item.id,
    //             // productTitle: itemData.item.title})

    //             selectItemHandler(itemData.item.id, itemData.item.title)
    // }} 
    // onAddToCart={()=>{
    //     // dispatching our action
    //     // cartActions.addToCart() takes our product
    //     dispatch(cartActions.addToCart(itemData.item))

    // }}
    >
{/* is selfclosing componento padarem i ne selfclosing ziuret i ProductItem'a props.childer*/}
        {/* <Button 
            color={Colours.price}
            title="View Details" 
            onPress={()=>{selectItemHandler(itemData.item.id, itemData.item.title)}
 } />
            <Button 
            color={Colours.price}
            title="to Cart" 
            onPress={()=>{dispatch(cartActions.addToCart(itemData.item))}}
            /> */}
    </ProductComponent>
}/> ;
}
WishListView.navigationOptions=navData=>{
    return{
    // headerTitle: 'Back',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Back" iconName={Platform.OS === 'android' ? 'md-arrow-back' : 'ios-arrow-back'}
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.navigate('ProductsOverview');
    }}/>
    </HeaderButtons>),
    // adding cart icon
    // headerRight: (
    // <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    //     <Item title="Cart" iconName="md-cart" 
    //     onPress={()=>{
    //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
    //         navData.navigation.navigate('Cart');
    //     }}/>
    // </HeaderButtons>
    // )
}
};

const styles= StyleSheet.create({
    centered:{
        flex:1, 
        justifyContent:'center', 
        alignItems:'center'
    }
})

export default WishListView;
