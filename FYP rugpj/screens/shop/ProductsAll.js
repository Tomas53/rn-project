import React , {useState, useEffect, useCallback}from 'react';
import {FlatList,Text,Button,Platform, View, StyleSheet} from 'react-native';
// allows us to tap into redux store and get out our products
// from there
import {useSelector, useDispatch} from 'react-redux';

import ProductComponent from './ProductComponent';
import Colours from '../../constants/Colours'
import * as cartActions from '../../store/actions/cart'
import * as wishListActions from '../../store/actions/wishList'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';
import * as productsActions from '../../store/actions/products'
import  Colors from '../../constants/Colours';


const ProductsAll=props=>{
    // const [isLoading, setIsLoading]=useState(false);//using this state for loading

    const [error, setError]=useState();//state for errors

    const [isRefreshCircle, setIsRefreshCircle]=useState(false);

    // sueselector takes a function which automatically receives
    // the state (redux state) as an input and which then returns
    // whichever data we wan to use

    // state.products refering to name provided in App.js (rootreducer) 
    const prodAvail =useSelector(state=>state.products.availableProducts);
  
    const disp=useDispatch();
//useCallBack helps us avoid infinite error
    const loadProd = useCallback( async () =>{
        setError(null);//setting error to null before reloading products
        // setIsLoading(true);
        setIsRefreshCircle(true)
        try{

        
        await disp(productsActions.fetchProducts());//trggers http request
    } catch(err){// we get the err from product actions because we rethrow it in catch section
        setError(err.message)//error is set to the message

    }
    setIsRefreshCircle(false)
       // setIsLoading(false)
    },[disp,  setError]);


    //onl works when the component has rendered the first time, we need useEffect code below
    useEffect(()=>{
        const willFocusSub=props.navigation.addListener('willFocus', loadProd);//didFocus works after a page was fully focused, willFocus will work when transition begins, willBlur work when we about to leave it, didBlur work once you are done

        return()=>{
            willFocusSub.remove();

        }
    },[loadProd]);

    useEffect(()=>{
        // setIsLoading(true)
        loadProd().then(()=>{
            // setIsLoading(false)
        });
    },[disp, loadProd]);//it will only rerun when this components is loaded basicalyy, when we got to products page
    
    const selectProdHandler=(id, title)=>{
        
        props.navigation.navigate('ProductInfo',
       
        {
        productId: id,
        productTitle: title})

    };

    if(error){//jei true td sugeneruoja mygtuka useriui pareloadint pagea
        return ( <View style={styles.prodCenter}>
           
        <Text>Error occured</Text>
        <Button title='reload' onPress={loadProd} color={'#17c4bd'}></Button>

        </View>
)
    }

    // if(isLoading){//tikrina ar true jei taip tai loadina coda below
    //     return ( <View style={styles.centered}>
    //                     {/* loading zenkliukas */}
    //         <ActivityIndicator size='large' color={Colors.primary}/>

    //     </View>
    //     )
    // }
    if( prodAvail.length===0){//tikrina ar yra produktu ir ar nebeloadina, nes jei nera tada meta sita teksta
        return ( <View style={styles.prodCenter}>
        <Text>No products found</Text>
        </View>
)
    }
        
    
    

    return <FlatList 
    style={styles.allProd}
    onRefresh={loadProd}
    refreshing={isRefreshCircle}
    data={prodAvail} 
    keyExtractor={item=>item.id}
   

    renderItem={itemData=>
    <ProductComponent 
    image={itemData.item.imageUrl}
    title={itemData.item.title}
    price={itemData.item.price}
    onSelect={()=>{
               
                selectProdHandler(itemData.item.id, itemData.item.title)
    }} 
   
    >
        <Button 
        // color='#17c4bd'
            // color={Colours.price}
            title="Product details" 
            onPress={()=>{selectProdHandler(itemData.item.id, itemData.item.title)}
 } />
            <Button style={styles.button}
            // color='#17c4bd'
            // color={Colours.price}
            title="To cart" 
            onPress={()=>{disp(cartActions.addToCart(itemData.item))}}
            />
             <Button style={styles.button}
            //  color='#17c4bd'
            // color={Colours.price}
            title="To wishlist" 
            onPress={()=>{disp(wishListActions.createProduct( itemData.item.title,itemData.item.description,itemData.item.imageUrl, itemData.item.price)&&loadProd)}}
            />
    </ProductComponent>
}/> ;
}
ProductsAll.navigationOptions=navData=>{
    return{
    headerTitle: 'All Products',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
        <Item title="Wish List" iconName={Platform.OS === 'android' ? 'md-heart' : 'ios-heart'}
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.navigate('WishList');
        }}/>
    </HeaderButtons>
    )
}
};

const styles= StyleSheet.create({
    prodCenter:{
        flex:1, 
        justifyContent:'center', 
        alignItems:'center'
    },
    allProd:{
        backgroundColor:"#c2fcf5"
    },
    button:{
        backgroundColor:"#6136c7"
    }
})

export default ProductsAll;