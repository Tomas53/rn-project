import React from 'react';
import {HeaderButton  } from 'react-navigation-header-buttons';
import {Ionicons} from '@expo/vector-icons';
import {Platform} from 'react-native';



const CustomButton=props=>{
// receives all the props (...props)
return( 

    <HeaderButton {...props} 
    IconComponent={Ionicons} 
    iconSize={30} color={"#dedb4e"}/>
)
};
export default CustomButton;//jkjk