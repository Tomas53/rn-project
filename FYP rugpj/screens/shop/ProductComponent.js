
import React from 'react';
import {View, Text, Image, StyleSheet,Button, TouchableOpacity } from 'react-native';

import Colours from '../../constants/Colours'

// cia bus tiks stylingas componento
const ProductComponent=props=>{
    return (
    <TouchableOpacity onPress={props.onSelect}>
    <View style={styles.product}>

        <View style={styles.imageContainer}>
        {/* gaunam duomenis per propsus */}
        <Image style={styles.imageStyle }source={{uri: props.image}}/>
        </View>
        <View style={styles.detailsStyle}>
        <Text style={styles.titleStyle }>{props.title}</Text>
        {/* tofixed padaro du skaicius po kablelio */}
        <Text style={styles.priceStyle }>£{props.price.toFixed(2)}</Text>
        </View>
        <View style={styles.actions}>
            {/*  iskvieciam funkcija per propsus*/}
            {/* <Button 
            color={Colours.price}
            title="View Details" 
            onPress={props.onViewDetail}
            />
            <Button 
            color={Colours.price}
            title="to Cart" 
            onPress={props.onAddToCart}
            /> */}
            {props.children}
            {/* special prop that adds whatever we pass between opening and closing tag in our case thus we can customize product for different screens/uses */}
        </View>
    </View>
    </TouchableOpacity>
    )



};


const styles =StyleSheet.create({
    product:{
        shadowColor: 'black',
        shadowOpacity: 0.15,
        shadowRadius:7,
        elevation: 50,
        //borderRadius:10,
        backgroundColor: '#F5EF9D',
        height: 400,
        margin:15

    },
    priceStyle:{
        fontSize:14,
        color: '#006161'
    },
    imageStyle:{
        width: "100%",
        height: '100%'

    },
    titleStyle:{
        fontSize:18,
        marginVertical:2,
    },
   
    actions:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '22%',
        paddingHorizontal:20


    },
    detailsStyle:{
        alignItems:"center",
        height: '12%',
        padding: 5
    },
    imageContainer:{
        width: "100%",
        height: '70%',
        // makes corner rounded
        // borderTopLeftRadius:10,
        // borderTopRightRadius:10,
        // child cant overlap
        overflow: 'hidden'

    }
});


export default ProductComponent;