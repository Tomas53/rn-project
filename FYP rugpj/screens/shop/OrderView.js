import React, {useEffect} from 'react';
import {View, FlatList, Text,StyleSheet} from 'react-native';
import { useSelector, useDispatch} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';
import OrderProd from './OrderProd'

import * as actionsOrder from '../../store/actions/orders'

const OrdersView=props=>{
    const orderDatabase =useSelector(state=>state.orders.orders)//accesing action order

    const disp= useDispatch();

    useEffect(()=>{
        disp(actionsOrder.getOrders())
    },[disp])

   // useEffect
    if(orderDatabase.length===0){
        return (<View>
            <Text>You have no orders</Text>
        </View>)
    }
        return (
        <FlatList 
        style={styles.OrderProd}
        data={orderDatabase} 
        keyExtractor={i=>i.id} 
        renderItem={itemInfo=><OrderProd amount={
            itemInfo.item.totalAmount} 
            date={itemInfo.item.normalizedDate}
            items={itemInfo.item.items}//we passing itms into order thus we can output items of our orders
            // style={{color: 'blue', fontSize: 30}}
            />}/>
        
        )
    };
    const styles=StyleSheet.create({
        orderProd:{
            shadowColor: 'black',
            shadowOpacity: 0.26,
            shadowOffset:{width:0, height:2},
            shadowRadius: 8,
            elevation:6,
            borderRadius:11,
            backgroundColor: 'white',
            margin: 21,
            padding:10,
            alignItems: 'center',
            borderBottomWidth:12,
            borderColor:'#F5EF9D'
    
    
        },
        summary:{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '100%',
            marginBottom:16
        },
        totalAmount:{
            
            fontSize: 15
        },
        date:{
            fontSize: 15
    
        }
    
    });
    
    OrdersView.navigationOptions=navOp=>{
    return{
        headerTitle: 'Orders',
        headerLeft: (
        <HeaderButtons HeaderButtonComponent={HeaderIcon}>
            <Item title="Menu" iconName="md-menu" 
            onPress={()=>{
                // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
                navOp.navigation.toggleDrawer();
            }}/>
            </HeaderButtons>),
    };
    
}
export default  OrdersView;