import React from 'react';
import {
  View,
  StatusBar,
  Image,
  Alert,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  StyleSheet,
  Clipboard,
} from 'react-native';
import {
  GiftedChat,
  Bubble,
  Send,
  SystemMessage,
  InputToolbar,
} from 'react-native-gifted-chat';
import { Dialogflow_V2 } from 'react-native-dialogflow-text';
import { dialogflowConfig } from '../../env';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';
// const BOT_USER = {
//   _id: 2,
//   name: 'ChatBot',
//   //avatar: 'https://www.b7web.com.br/avatar3.png',
// };

const BOT_USER = {
  _id: 2,
  name: 'FAQ Bot',
  avatar: 'https://i.imgur.com/7k12EPD.png'
};
export default class ChatBotScreen extends React.Component {
  constructor(props) {
    super(props);
    // let firstMsg = {
    //   _id: 1,
    //   text: 'Chat support',
    //   createdAt: new Date(),
    //   system: true,
    //   user: BOT_USER,
    // };

    this.state = {
      messages: [{
        _id: 1,
        text: `Hi! I am the Chatbot .\n\nHow could I help you?`,
        createdAt: new Date(),
        user: BOT_USER
      }],
    };
  }

  componentDidMount() {
    Dialogflow_V2.setConfiguration(
      dialogflowConfig.client_email,
      dialogflowConfig.private_key,
      Dialogflow_V2.LANG_ENGLISH_US,
      dialogflowConfig.project_id
    );
  }

  updateBot(text) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      createdAt: new Date(),
      user: BOT_USER,
    };
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg]),
    }));
  }

  handleDialogflowResponse(result) {
    console.log(result);
    let text = result.queryResult.fulfillmentMessages[0].text.text[0];
    this.updateBot(text);
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
    let message = messages[0].text;

    Dialogflow_V2.requestQuery(
      message,
      result => this.handleDialogflowResponse(result),
      error => console.log(error)
    );
  }

  // renderBubble = props => {
  //   return (
  //     <Bubble
  //       {...props}
  //       wrapperStyle={{
  //         left: {
  //           backgroundColor: '#e5e5e5',
  //         },
  //         right: {
  //           backgroundColor: '#FF3D00',
  //         },
  //       }}
  //     />
  //   );
  // };
  renderSend(props) {
    return (
      <Send {...props}>
        <View
          style={{
            marginTop: 2,
            marginRight: 10,
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontSize: 17,
              fontWeight: 'bold',
              marginBottom: 8,
              color: '#FF3D00',
            }}>
            Send
          </Text>
        </View>
      </Send> 
    );
  }
  // onLongPress(context, message) {
  //   console.log(context, message);
  //   const options = ['Copy','Delete', 'Cancel'];
  //   const cancelButtonIndex = options.length - 1;
  //   context.actionSheet().showActionSheetWithOptions(
  //     {
  //       options,
  //       cancelButtonIndex,
  //     },
  //     buttonIndex => {
  //       switch (buttonIndex) {
  //         case 0:
  //           // Your delete logic
  //           //Clipboard.setString(message);
  //           break;
  //         case 1:
  //           // Your delete logic
  //           break;
  //         default:
  //           break;
  //       }
  //     }
  //   );
  // }
  renderSystemMessage = props => {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 10,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <StatusBar hidden />
        <View style={{ backgroundColor: '#fff' }}>
          <TouchableOpacity>
            {/*Donute Button Image */}
            <Image
              
              style={{
                marginLeft: 10,
                marginTop: 10,
                width: 25,
                height: 25,
                tintColor: '#FF3D00',
              }}
            />
          </TouchableOpacity>
          <Text style={{ fontSize: 15, color: '#aaa', textAlign: 'center' }}>
            💬Please enter your questions
          </Text>
        </View>
        <GiftedChat
         // onLongPress={this.onLongPress}
          renderLoading={() => (
            <ActivityIndicator size="large" color="#FF3D00" />
          )}
          renderSystemMessage={this.renderSystemMessage}
          renderAvatar={null}
          renderSend={this.renderSend}
          //renderBubble={this.renderBubble}
          inverted={true}
          locale="pt-BR"
          onPressAvatar={() => Alert.alert(' This is chat support', 'Ok')}
          placeholder={'Message'}
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
        />
      </View>
    );
  }
  
}
ChatBotScreen.navigationOptions=navData=>{
  return{
  headerTitle: 'Chat support',
  headerLeft: (
  <HeaderButtons HeaderButtonComponent={HeaderIcon}>
  <Item title="Menu" iconName="md-menu" 
  onPress={()=>{
      // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
      navData.navigation.toggleDrawer();
  }}/>
  </HeaderButtons>),
  // adding cart icon
  // headerRight: (
  // <HeaderButtons HeaderButtonComponent={HeaderIcon}>
  //     <Item title="Cart" iconName="md-cart" 
  //     onPress={()=>{
  //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
  //         navData.navigation.navigate('Cart');
  //     }}/>
  // </HeaderButtons>
  // )
}
};

