import React, {useState,useEffect, useCallback, useReducer} from 'react'
import { View, Button, Image, Text, StyleSheet, Alert,ScrollView, KeyboardAvoidingView, ActivityIndicator} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderIcon from './HeaderIcon';

import Colors from '../../constants/Colours'


import ProductComponent from './ProductComponent';
import { TextInput } from 'react-native-gesture-handler';

const ImgPicker = props => {
    const [pickedImage, setPickedImage] = useState();
    const [pickedTitle, setPickedTitle]= useState('')

    const [pickedPrice, setPickedPrice]= useState(0)
  
  
    const verifyPermissions = async () => {
      const result = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (result.status !== 'granted') {
        Alert.alert(
          'Insufficient permissions!',
          'You need to grant camera permissions to use this app.',
          [{ text: 'Okay' }]
        );
        return false;
      }
      return true;
    };
  
    const takeImageHandler = async () => {
      const hasPermission = await verifyPermissions();
      if (!hasPermission) {
        return;
      }
      const image = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [10, 10],
        quality: 0.6
      });
  
      setPickedImage(image.uri);
    };
   


       
  
    return (
      <View style={styles.centered}>
        <View style={styles.imagePreview}>
          {!pickedImage ? (
            <Text>No image picked yet.</Text>
          ) : (
            <Image style={styles.imageContainer} source={{ uri: pickedImage }} />
          )}
        </View>
        <Button
          title="Take Image"
         // color={Colors.primary}
          onPress={takeImageHandler}
        />            
                <View style={styles.formControl}>
                <Text style={styles.label}>Title</Text>
                    <TextInput
                    label="title"
                    style={{height: 40}}
                    placeholder="Title"
                    onChangeText={pickedTitle => setPickedTitle(pickedTitle)}
                    defaultValue={pickedTitle}
                    />   
                <View style={styles.formControl}>
                    <Text style={styles.label}>Price</Text>
                </View>
                    <TextInput
                    onChangeText={pickedPrice => setPickedPrice(pickedPrice)}
                      keyboardType={'numeric'}
                    />
                </View>
                <ProductComponent  
                style={styles.centered}
                image={pickedImage}
                title={pickedTitle}
                price={parseInt(pickedPrice)}
                />
    

      </View>
 
      
    );
  };
  
  const styles = StyleSheet.create({
    imagePicker: {
      alignItems: 'center'
    },
    imagePreview: {
      width: '100%',
      height: 200,
      marginBottom: 10,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: '#ccc',
      borderWidth: 1
    },
    image: {
      width: '100%',
      height: '100%'
    },
    imageContainer:{
      width: "100%",
      height: '60%',
      // makes corner rounded
      borderTopLeftRadius:10,
      borderTopRightRadius:10,
      // child cant overlap
      overflow: 'hidden'

  },
    centered:{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center',
      width: '100%',
      height: '100%'
  }
  });
  


ImgPicker.navigationOptions=navData=>{
    return{
    headerTitle: 'Pre view',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderIcon}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    // headerRight: (
    // <HeaderButtons HeaderButtonComponent={HeaderButton}>
    //     <Item title="Cart" iconName="md-cart" 
    //     onPress={()=>{
    //         // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
    //         navData.navigation.navigate('Cart');
    //     }}/>
    // </HeaderButtons>
    // )
  
  }
};

export default ImgPicker