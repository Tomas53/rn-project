import moment from 'moment';
class OrderSchema {

    constructor(id, items, totalAmount, date){
        this.id=id;//order has standalone id
        this.items=items;
        this.totalAmount=totalAmount;//total items
        this.date=date;//date when order created
    }
    get normalizedDate(){//kad galetume laika atvaizduot ordriuose
        return this.date.toLocaleDateString('en-EN',{
            year:'numeric',
            month:'long',
            day:'numeric',
            // hour:'2-digit',
            // minute:'2-digit'

        })
       // return moment(this.date).format('MMMM Do YYYY');//graziai suformatuoja laika androide ir iose
    }
}

export default OrderSchema;