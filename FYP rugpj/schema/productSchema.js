class ProductSchema{
    constructor(id, ownerId, title, imageUrl, description, price){
        // storing the data we recived in the properties
        // of the created object
        this.id=id;
        this.ownerId=ownerId;
        this.title=title;
        this.imageUrl=imageUrl;
        this.description=description;
        this.price=price;

    }
}
export default ProductSchema;