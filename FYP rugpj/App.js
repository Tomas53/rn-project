import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import productsReducer from './store/reducers/products';
import ShopNavigator from './navigation/ShopNavigator';
import cartReducer from './store/reducers/cart'
import ordersReducer from './store/reducers/orders'
import ReduxThunk from 'redux-thunk';
import authReducer from './store/reducers/auth'
import wishListReducer from './store/reducers/wishList'
const rootReducer =combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer,//lets us to dispatch actions
  auth: authReducer,// adding auth reducer to the root reducer with the auth key, this lest us to acces our tokens
  wishList: wishListReducer
})

const store=createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
      <Provider store={store}>
        <ShopNavigator/>
      </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
