import {AsyncStorage} from 'react-native';

export const SIGNUP='SIGNUP';//action identifier for action which we later want to handle in our store
export const LOGIN='LOGIN';
export const AUTHENTICATE='AUTHENICATE';
export const LOGOUT = 'LOGOUT';


export const authenticate=(userId, token)=>{
    return{type: AUTHENTICATE, userId:userId, token:token}
}

export const signup = (email, password) => {// we need action creator signup to have at least one used in order to login
    return async dispatch => {//we are returning a function which can use async await which gets dispatch function as an argument passed by the redux thunk middleware
                             //this allows us to run async code before we dispatch an action which actually reaches our store
    
        const response=await fetch(//wawit lets us wait for server response
            //url we are sending request to
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCqAySsPXqLG6p-MlY5owEtea0VFbbHhy0'
        ,
        // we are adding object{} here which lets to configure this request
        {
            // the docs we see that we need to send a POST request
            // also the request should have aheader 
            method: 'POST',
            headers:{
                'Content-Type':'application/json'// pasakom kad paduosim json tipo duomenis

            },
            body: JSON.stringify({// paduodam boyd json formatu
                // from the docs we see that body needs to have email, password and returnSecureToken
                email:email,
                password:password,
                returnSecureToken:true
            })
        }
        );
        // SIGNUPPPP
       // from fire base we are getting different types of error thus we want to make a different type o error meassages according to those firebase eroror messages
       if(!response.ok){
        const errReturned= await response.json();
        const errorId=errReturned.error.message// we are extracting error message

        let errMess = 'something is not right'
        if(errorId==='EMAIL_EXISTS'){
            errMess='The email you are trying to use already exists';

        }
        
        throw new Error(errMess)

        // throw new Error('Someting went wrong')
    }
        const resData = await response.json();// jei viskas su responu gerai tai einam i sita eilute kurioj mes laukiame responso kur response.json(); upacks server response body and aoutomatically transforms it from json to javascript (javascript object or arry)
        console.log(resData);
        //we get idToken and localId from the resData from the firebase response
        dispatch(authenticate(resData.localId, resData.idToken))
        //dispatch({ type: SIGNUP, token:resData.idToken, userId: resData.localId});//we are dispatching this action object
       
         const expirationDate=new Date(new Date().getTime()+parseInt(resData.expiresIn)*1000)
         saveDataToStorage(resData.idToken, resData.localId, expirationDate)
                     
    
    }
}

export const login = (email, password) => {// we need action creator signup to have at least one used in order to login
    return async dispatch => {//we are returning a function which can use async await which gets dispatch function as an argument passed by the redux thunk middleware
                             //this allows us to run async code before we dispatch an action which actually reaches our store
    
        const response=await fetch(//wawit lets us wait for server response
            //url we are sending request to
            'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCqAySsPXqLG6p-MlY5owEtea0VFbbHhy0'
        ,
        // we are adding object{} here which lets to configure this request
        {
            // the docs we see that we need to send a POST request
            // also the request should have aheader 
            method: 'POST',
            headers:{
                'Content-Type':'application/json'// pasakom kad paduosim json tipo duomenis

            },
            body: JSON.stringify({// paduodam boyd json formatu
                // from the docs we see that body needs to have email, password and returnSecureToken
                email:email,
                password:password,
                returnSecureToken:true
            })
        }
        );

        // from fire base we are getting different types of error thus we want to make a different type o error meassages according to those firebase eroror messages
        if(!response.ok){
            const errReturned= await response.json();
            const errorId=errReturned.error.message// we are extracting error message

            let errMess = 'something is not right'
            if(errorId==='EMAIL_NOT_FOUND'){
                errMess='This email does not exist';

            }else if(errorId==='INVALID_PASSWORD'){
                errMess='Invalid password'
            }
            
            throw new Error(errMess)

            // throw new Error('Someting went wrong')
        }
        const resData = await response.json();// jei viskas su responu gerai tai einam i sita eilute kurioj mes laukiame responso kur response.json(); upacks server response body and aoutomatically transforms it from json to javascript (javascript object or arry)
        console.log(resData);
       // we need this token below which we are now storing to access firebase api
        dispatch(authenticate(resData.localId, resData.idToken))
       // dispatch({ type: LOGIN, token:resData.idToken, userId: resData.localId});//we are dispatching this action object
        const expirationDate=new Date(new Date().getTime()+parseInt(resData.expiresIn)*1000)// takes current date and adds the expiry time of the token just to know when the token will expire
        saveDataToStorage(resData.idToken, resData.localId, expirationDate)// calling saveDataToStorage after we done logging in or sining up
        // dispatch({ type: LOGIN });//we are dispatching this action object
                    }

                    // initial firebase rules code
                    // {
                    //     "rules": {
                    //       ".read": true,
                    //       ".write": true
                    //     }
                    //   }
                    // only authenticated users are able to write(only users who send request with the valid token)
                    // {
                    //     "rules": {
                    //       ".read": true,
                    //       ".write": "auth!=null"
                    //     }
                    //   }
}
const saveDataToStorage=(token, userId, expirationDate)=>{
    AsyncStorage.setItem('userData', JSON.stringify({//saving this object to the device
        token:token,
        userId: userId,
        expiryDate: expirationDate.toISOString()

    }))
}
export const logout=()=>{

    return {type: LOGOUT};//action identifier
}

// const saveDataToStorage=(token, userId, expirationDate)=>{
//     AsyncStorage.setItem('userData', JSON.stringify({// saving data, however the data we saving needs to be a string
//         token: token,
//         userId:userId,
//         expiryDate: expirationDate.toISOString()//tiISOString converts into a stringa
//     }))
// }