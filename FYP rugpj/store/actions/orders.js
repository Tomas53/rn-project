//import Order from '../../models/order'
import Order from '../../schema/orderSchema'
export const ADD_ORDER='ADD_ORDER';//identifier


export const SET_ORDERS='SET_ORDERS';// to set the loaded orders



export const getOrders=()=>{
    return async (disp, getState)=>{
        const userId= getState().auth.userId;// geting current user id



        try{
            const userData = await fetch(`https://rn-fyp.firebaseio.com/orders/${userId}.json`, {
                //by default fetch executes GET http request thus we dont use method to specify the request
                 //we also dont use headers for GET request
               //we cant set a body either
     
            });


            if(!userData.ok){//looks if the userData is false (if its wronf in 200 code error range)
                throw new Error('Error 200')
            }

           const resData=await userData.json();
           const ordersReceived=[];//we are woring with array thus we need to transform firebase json respone to an array                 
            for (const i in resData){
                ordersReceived.push(
                    new Order(
                        i, 
                        resData[i].cartItems,
                         resData[i].totalAmount,
                          new Date(resData[i].date))//.we need to create a new date object because resData[key].date is just a date string in firebase and we need an object thus we wrap it into date object
                )   
            }
            disp({type: SET_ORDERS, orders: ordersReceived})
           
        }
         catch(err){
             throw err;
         }

       
    }

}


export const addOrder= (cartItems, totalAmount)=>{//recieves cartitems and total amount
    

        return async (disp, getState)=>{
            const token=getState().auth.token//getState() gets acces to our full redux store, .auth gives us acces to auth slice,  .token gives us acces to the token property which we are managing in that auth slice
            const userId= getState().auth.userId;// geting current user id

            const date= new Date();
            //adding user id to our dynamic url
            const userData = await fetch(`https://rn-fyp.firebaseio.com/orders/${userId}.json?auth=${token}`, {//fetch can be used for get,post,put any king of http request, we are storing userData constant with await keyword, behind the scense it transforms code into old syntax with then()
            method:'POST',
               headers:{
                   'Content-Type': 'application/json'            },
               body:JSON.stringify({//using stringify to turn js object into json format(into a string in the end) 
                   cartItems,
                   totalAmount,
                   date: date.toISOString()//creating date loacly in app and then sending it into server
               })
           });

           if(!userData.ok){
               throw new Error('Something went wrong');
           }

           const resData=await userData.json();//userData can be unpacked to get the data in it, thus we use userData.json(), await is here because it is an async task
                                            //this gives us data return back when we add a product in firebase
        //the dispach function below will only be dispatched when  all await functions abowe will be executed, because of the await
       


        disp({
            type: ADD_ORDER, //id: resData.name is server generated id which we get through userData
            orderData:{id: resData.name, items: cartItems, amount: totalAmount, date: date//visi sitie parametrai paduodami reduceriui
            //order data key where we merge cart items and total amount          
            }
            })

    }

}