//import Profile from '../../models/profile'
import Profile from '../../schema/profileSchema'
export const ADD_PROFILE='ADD_ORDER';//identifier


export const SET_ORDERS='SET_ORDERS';// to set the loaded orders



export const fetchProfile=()=>{
    return async (disp, getState)=>{
        const userId= getState().auth.userId;// geting current user id



        try{
            const response = await fetch(`https://rn-fyp.firebaseio.com/profiles/${userId}.json`, {
                //by default fetch executes GET http request thus we dont use method to specify the request
                 //we also dont use headers for GET request
               //we cant set a body either
     
            });


            if(!response.ok){//looks if the response is false (if its wronf in 200 code error range)
                throw new Error('Something went wrong')
            }

           const resData=await response.json();
           const loadedProfiles=[];//we are woring with array thus we need to transform firebase json respone to an array                 
            for (const i in resData){
                loadedProfiles.push(
                    new Order(
                        i, 
                        resData[i].cartItems,
                         resData[i].Amount,
                          new Date(resData[i].date))//.we need to create a new date object because resData[key].date is just a date string in firebase and we need an object thus we wrap it into date object
                )   
            }
            disp({type: SET_ORDERS, orders: loadedOrders})
           
        }
         catch(err){
             throw err;
         }

       
    }

}


export const addProfile= (name, description, image)=>{//recieves cartitems and total amount
    

        return async (disp, getState)=>{
            const token=getState().auth.token//getState() gets acces to our full redux store, .auth gives us acces to auth slice,  .token gives us acces to the token property which we are managing in that auth slice
            const userId= getState().auth.userId;// geting current user id

            const date= new Date();
            //adding user id to our dynamic url
            const response = await fetch(`https://rn-fyp.firebaseio.com/profiles/${userId}.json?auth=${token}`, {//fetch can be used for get,post,put any king of http request, we are storing response constant with await keyword, behind the scense it transforms code into old syntax with then()
            method:'POST',
               headers:{
                   'Content-Type': 'application/json'            },
               body:JSON.stringify({//using stringify to turn js object into json format(into a string in the end) 
                   name,
                   description,
                   image//creating date loacly in app and then sending it into server
               })
           });

           if(!response.ok){
               throw new Error('Something went wrong');
           }

           const resData=await response.json();//response can be unpacked to get the data in it, thus we use response.json(), await is here because it is an async task
                                            //this gives us data return back when we add a product in firebase
        //the dispach function below will only be dispatched when  all await functions abowe will be executed, because of the await
       


        disp({
            type: ADD_ORDER, //id: resData.name is server generated id which we get through response
            orderData:{id: resData.name, items: cartItems, amount: Amount, date: date//visi sitie parametrai paduodami reduceriui
            //order data key where we merge cart items and total amount          
            }
            })

    }
}