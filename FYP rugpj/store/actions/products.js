// import ProductsOverviewScreen from "../../screens/shop/ProductsOverviewScreen";
import Product from '../../schema/productSchema'
export const DELETE_PRODUCT ='DELETE_PRODUCT';//identifier DELETE_PRODUCT
export const CREATE_PRODUCT ='CREATE_PRODUCT';
export const SET_PRODUCTS = 'SET_PRODUCTS'; //will be used to work with products from firebase 

export const UPDATE_PRODUCT ='UPDATE_PRODUCT';


export const fetchProducts= ()=>{// we dont have an identifier here because we wont dispatch this action that reaches a reducer, it will be action with http request and async code
   // it returns function which gets dispatch function as an argument
    return async (disp, getState)=>{
        const userId= getState().auth.userId;// geting current user id


        try{
            const response = await fetch('https://rn-fyp.firebaseio.com/products.json', {
                //by default fetch executes GET http request thus we dont use method to specify the request
                 //we also dont use headers for GET request
               //we cant set a body either
     
            });


            if(!response.ok){//looks if the response is false (if its wronf in 200 code error range)
                throw new Error('Error 200')
            }

           const resData=await response.json();
           const loadedProducts=[];//we are working with array thus we need to transform firebase json respone to an array                 
            for (const i in resData){
                loadedProducts.push(new Product(//push ads new product to loadedProducts array
                i, 
                resData[i].ownerId, 
                resData[i].title,
                 resData[i].imageUrl,
                 resData[i].description,
                 resData[i].price
                ))
            }
           
           //filtering the products to have only the owner products
           disp({type: SET_PRODUCTS, products:loadedProducts, userProducts: loadedProducts.filter(prod=>prod.ownerId===userId) })//we added loadeproducts array to action(SET_PRODUCTS) we dispatch
       
        } catch(err){
            //can do witherror whaever we want
            throw err;
        }
         }
}


export const createProduct=(title, description, imageUrl, price)=>{//createProdut is action creator( a function that just returns an action)
    return async (disp, getState)=>{//we add async for async functionality
        // thank to reduxThunk insted of returning an action we can return a function
        //this function receives dispatch function as an argument, and then dispatches an action (insted of returning the action we are dispatching it)
       //we changed createProduct to be a function, that we can dispatch from inside of our components
       //which now still works as before, but thanks to reduxthunk the alternative syntax is supported
       //where createProduct action function does not immideatly return action object, but insted it returns a function
       //and if it returns it, then reduxtThunk steps in to make sure everything works
       //in the end reduxtThunk will call dispatch function for us, and therefore we can dipatch a new action and dipatch actual action object
       //but before we do that we can execute any async code we want and that wont brake the redux flow because reduxThunk will take care for it
       const token=getState().auth.token//getState() gets acces to our full redux store, .auth gives us acces to auth slice,  .token gives us acces to the token property which we are managing in that auth slice
       const userId= getState().auth.userId;// geting current user id

        const response = await fetch(`https://rn-fyp.firebaseio.com/products.json?auth=${token}`, {//fetch can be used for get,post,put any king of http request, we are storing response constant with await keyword, behind the scense it transforms code into old syntax with then()
           // firebase transforms our request to database query or structure, thus we add products.json to our link to make firebase to make a node for us and store data in there
        method:'POST',//we specifying for firebase that this is post request
           headers:{
               'Content-Type': 'application/json'//lets firebase to know that we are about to send some json data
           },
           body:JSON.stringify({//using stringify to turn js object into json format(into a string in the end) 
               title,
               description,
               imageUrl,
               price,
               ownerId: userId// mapping user id to order
           })
       });
       //fetch return a promise, thus we can add .then(response=>{...}) after fetch, thus we can do smth with that response
       //we can listen to erros with catch()
       // however we use more modern way like async await for these functionalities
       const resData=await response.json();//response can be unpacked to get the data in it, thus we use response.json(), await is here because it is an async task
                                        //this gives us data return back when we add a product in firebase
    //the dispach function below will only be dispatched when  all await functions abowe will be executed, because of the await
    disp({
            type: CREATE_PRODUCT,
            
            productData:{
                id: resData.name,// we are dding id here, because we want to use firebase generated id, which we take from resData, which we get from firebase response(await) 
                title: title,//title mapped to title above
                description: description,//description mapped to description above and so on
                imageUrl: imageUrl,
                price: price,
                ownerId: userId
            }
    }
       )
  

    }
}

export const deleteProduct= productId=>{//we are exporting action creator deleteProduct
    return async (disp, getState)=>{
        const token=getState().auth.token//getState() gets acces to our full redux store, .auth gives us acces to auth slice,  .token gives us acces to the token property which we are managing in that auth slice

        const response=await fetch(`https://rn-fyp.firebaseio.com/products/${productId}.json?auth=${token}`, {
           
        method:'DELETE',
           headers:{
               'Content-Type': 'application/json'//lets firebase to know that we are about to send some json data
           }
          
       });
       if (!response.ok){
        throw new Error('Error')
    }
    disp( { type: DELETE_PRODUCT, pid: productId });//actioncreator return action object, pid is product id thus we use forwarded productId
 }
}
export const updateProduct=(id, title, description, imageUrl)=>{//here we have id to identify wich product we are updating, we dont have price because we chose to make price not udatable
    

        return async (disp, getState)=>{// getState lets us to acces our state in redux store
            const token= getState().auth.token;//getState() gets acces to our full redux store, .auth gives us acces to auth slice,  .token gives us acces to the token property which we are managing in that auth slice
            const response= await fetch(`https://rn-fyp.firebaseio.com/products/${id}.json?auth=${token}`, {//we are usig backticks instead of single quates whic lets us to use dynamic syntax
               // we dynamically inserted id thus now we are targeting a specific product in adatabase
            method:'PATCH',//PUT will fully overwrite the resource with new data, PATCH updates it in the places you tell it to update thus it updates just a specific fields
               headers:{
                   'Content-Type': 'application/json'//lets firebase to know that we are about to send some json data
               },
               body:JSON.stringify({//using stringify to turn js object into json format(into a string in the end) 
                   title,
                   description,
                   imageUrl,
                   
               })
           });
           if (!response.ok){
               throw new Error('Something went wrong')
           }


        //before we trigger dipatch(code below) we can connect to our server and update data in datatbase there
        disp({
            type: UPDATE_PRODUCT,
            pid: id,
            productData:{
                title: title,
                description: description,
                imageUrl: imageUrl,
               
            }
        
    }
    )
    

    }
}