import {DELETE_PRODUCT, CREATE_PRODUCT, UPDATE_PRODUCT, SET_PRODUCTS} from '../actions/wishList'
// import Product from '../../models/product.js';
import Product from '../../schema/productSchema.js';

const initialState={
    // all products or products you havent created
    wishListProducts: [],// we start with empty array//PRODUCTS,
    // userCreaed products
    userProducts:[]//we start with empty array//PRODUCTS.filter(prod=>prod.ownerId==='u1')
    //userWishList:[]
};
export default (state=initialState, action)=>{
    switch(action.type){
        case SET_PRODUCTS:
            return{
                wishListProducts: action.products,//returning a state where availableproducts are equal to action.products(the products we derived in action creator (in loadedProducts array))
                userProducts: action.userProducts //action.products.filter(prod=>prod.ownerId==='u1')//filtering to only show user products
            }

        case CREATE_PRODUCT:
            const newProduct=new Product(
                action.productData.id,//we are using firebase generated id here
            // new Date().toString(), //dummy id
            action.productData.ownerId,//getting owner id from our action creator(products.js in action folder) 
            action.productData.title, //retrieving data from action
            action.productData.imageUrl, 
            action.productData.description,
            action.productData.price);
            return{
                ...state,
                wishListProducts: state.wishListProducts.concat(newProduct),//contact yra old array plus new element in our case newProduct
                userProducts: state.userProducts.concat(newProduct)
            }
        
        case DELETE_PRODUCT:
            return {//products needs to be deleted from userProducts array and from availableProducts array (from all app)
                ...state,
                userProducts: state.userProducts.filter(product=>//filter() return new array which is ran on function and if the function return true wee keep the item id it return false we dont keep the item
                    product.id!==action.pid// it will keep the products if the id doesnt match but if ittmacthes we then delete the product
                    ),
                    wishListProducts: state.wishListProducts.filter(product=>
                    product.id!==action.pid
                    ),
                
            };
    }

    return state;
}
