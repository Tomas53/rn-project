// import PRODUCTS from '../../data/dummy-data.js'
import {DELETE_PRODUCT, CREATE_PRODUCT, UPDATE_PRODUCT, SET_PRODUCTS} from '../actions/products'
// import Product from '../../models/product.js';
import Product from '../../schema/productSchema.js';
const initState={
    // all products or products you havent created
    availableProducts: [],// we start with empty array//PRODUCTS,
    // userCreaed products
    userProducts:[]//we start with empty array//PRODUCTS.filter(prod=>prod.ownerId==='u1')
    //userWishList:[]
};
export default (state=initState, action)=>{
    switch(action.type){
        case SET_PRODUCTS:
            return{
                availableProducts: action.products,//returning a state where availableproducts are equal to action.products(the products we derived in action creator (in loadedProducts array))
                userProducts: action.userProducts //action.products.filter(prod=>prod.ownerId==='u1')//filtering to only show user products
            }

        case CREATE_PRODUCT:
            const FreshProd=new Product(
                action.productData.id,//we are using firebase generated id here
            // new Date().toString(), //dummy id
            action.productData.ownerId,//getting owner id from our action creator(products.js in action folder) 
            action.productData.title, //retrieving data from action
            action.productData.imageUrl, 
            action.productData.description,
            action.productData.price);
            return{
                ...state,
                availableProducts: state.availableProducts.concat(FreshProd),//contact yra old array plus new element in our case FreshProd
                userProducts: state.userProducts.concat(FreshProd)
            }
        case UPDATE_PRODUCT:
            const prodNumber=state.userProducts.findIndex(prod=>prod.id===action.pid)//we are finding the index of the current product we want to update
            const updProduct=new Product(
                action.pid,
                state.userProducts[prodNumber].ownerId,//we need to have the same id of owner thus we dont change anything here
                action.productData.title, //however we store new title for example
                action.productData.imageUrl, //however we store new imageUrl for example
                action.productData.description,//however we store new description for example
                state.userProducts[prodNumber].price//price doesnt change
                
           )
            const updatedProdUser=[...state.userProducts];//here (updatedProdUser) we store copy od existing user products
            updatedProdUser[prodNumber]=updProduct;//from that array of updatedProdUser we find the product and set it to be equal to our newly udated product(updatedProduct)
            const availableProdNumber= state.availableProducts.findIndex(//we are finding the index of the current product we want to update from availableProducts array because the id is different from useproducts
                prod=>prod.id===action.pid
            )
            const updatedProdAvail=[...state.availableProducts];
            updatedProdAvail[availableProdNumber]=updProduct;//we replace existing product with the updated product in availableproducts array
            return {
                ...state,
                availableProducts: updatedProdAvail,
                userProducts: updatedProdUser
            }
        case DELETE_PRODUCT:
            return {//products needs to be deleted from userProducts array and from availableProducts array (from all app)
                ...state,
                userProducts: state.userProducts.filter(product=>//filter() return new array which is ran on function and if the function return true wee keep the item id it return false we dont keep the item
                    product.id!==action.pid// it will keep the products if the id doesnt match but if ittmacthes we then delete the product
                    ),
                availableProducts: state.availableProducts.filter(product=>
                    product.id!==action.pid
                    ),
                
            };
    }

    return state;
}
