import { ADD_ORDER, SET_ORDERS } from "../actions/orders";
// import Order from "../../models/order";
import Order from "../../schema/orderSchema";
const initialState={
    orders:[]
}

export default (state= initialState, action)=>{
    switch(action.type){
        case SET_ORDERS:
            return {
                orders: action.orders
            }
        case ADD_ORDER:

            // new order stored in a new order constant with the order model we created
            const newOrder=new Order(
            action.orderData.id,//as id , got from actions, from firebase
            action.orderData.items, 
            action.orderData.amount, 
            action.orderData.date//current time stamp
            );
            return {
                ...state,//copying the old state, which is useless because we dont
                         // have any other state, but in case we have more complex state in the future it will be usefull
                orders: state.orders.concat(newOrder)//cancat adds new item into array and returns new array that includes that item
                         // old array stays untouched new array gets returned
            };

    }

    return state;
    
};