
import {ADD_TO_CART, REMOVE_FROM_CART} from '../actions/cart'

import CartProdSchema from '../../schema/cartProdSchema'
import {ADD_ORDER} from '../actions/orders'
import {DELETE_PRODUCT} from '../actions/products'
// little reducers that are combined in a root reducer that make that big state in the end
// thus this is small sub state
const initState={
    // array of items
    // items:[],
    // items will be a js objects
    items: {},
    // number of items
    totalAmount: 0

}
// state is initState as an default value and
// action is as an argument
export default (state=initState, action)=>{
    switch(action.type){
        case ADD_TO_CART:
            const prodInCart=action.product;
            const priceOfProd=prodInCart.price;
            const titleOfProd=prodInCart.title;

            
            let updNewCartProd;


            // checking if we have already got the item

            if(state.items[prodInCart.id]){
                // aready have item in the cart
                updNewCartProd=new CartProdSchema(
                    // we get the quantity from the state items for the
                    // id of the added product and we look for quantity, thus
                    // we simply add one to that
                    state.items[prodInCart.id].quantity+1,
                    // cart item for a second argument takes the product price
                    priceOfProd,
                    // we are taking the most recent title here from
                    // the product we getting here->  const titleOfProd=prodInCart.title;
                    titleOfProd,
                    // we are taking the currrent sum plus the product price
                    state.items[prodInCart.id].sum+priceOfProd
                );
                // we can return the new state by copying the old state
                // here overwrite the [prodInCart.id] with our updatedCartItem
                // here we actually dont need to copy state(...state) because
                // we are updating its every piece with something(items[] and totalAmount),
                // however if we didint do that we would lose data that we are not updating
                // thus its better to copy state and have all the contents
                // return { ...state, items:{ ...state.items, [prodInCart.id]: updatedCartItem },
                //     totalAmount: state.totalAmount+priceOfProd
                // }
            } else{
                // adding a new item
                updNewCartProd=new CartProdSchema(1,priceOfProd, titleOfProd, priceOfProd);
            }
                // we return a copy of our tate(...state)
                // we set items equal to a new object where we copy our existing state
                // items in(items:{ ...state.items,) and then we add new key([prodInCart.id])
                // and the value is newCartItem
                return{
                   ...state,
                   items:{ ...state.items, [prodInCart.id]: updNewCartProd},
                    // totalAmount should be our old total amount plus priceOfProd
                    totalAmount: state.totalAmount+priceOfProd
                };
            
        case REMOVE_FROM_CART: 
        // we have two cases if there is one item in the cart we want to delete it
        // if there is two items in a cart we want to reduce its number
        // we have to define what our quantity is
        // state.items is an object thus we can use action.pid
           const cartProdChosen=state.items[action.pid];
           const currNum=cartProdChosen.quantity;
           
           let updProductsCart;// as a avriable
           if(currNum>1){
            //    in this case we are reducig quantity

            const updCartProd=new CartProdSchema(
                cartProdChosen.quantity-1, 
                cartProdChosen.productPrice,
                cartProdChosen.productTitle, 
                cartProdChosen.sum-cartProdChosen.productPrice
                );
                // updated cart item needs to replace current cart item in our object
                
                updProductsCart={...state.items, [action.pid]: updCartProd}//with state.item we passing old data, with updatedCartItem we replacing our old cart item with the updated quantity and sum
           }else{
            //here we bring back all items except from the item 
            // we want to reduce
            updProductsCart={...state.items};
            delete updProductsCart[action.pid];// will delete product item from copied javascript cart items object

           }
           return{
               ...state,
               items: updProductsCart,
               totalAmount: state.totalAmount - cartProdChosen.productPrice
           }
           case ADD_ORDER:
               return initState;// taip isvalom cart'a nes initial state nieko nera
           case DELETE_PRODUCT:
               if (!state.items[action.pid]){//checking if the item exists in a cart
                   return state;//jei positive tada jis compiliuoja apacioj esanti koda jei ne tei tsg grazina state kadangi nieks nepasikeite
               }
               const updProducts={...state.items};//here we copying existing state items
               const prodTotal=state.items[action.pid].sum;//getting total sum of items 
               delete updProducts[action.pid];//deleting item from the cart
               return {
                   ...state,
                   items: updProducts,//items is the updatedproducts without our deleted item
                   totalAmount: state.totalAmount - prodTotal//we deleting price of deleted item from our cart
               }

    }
    // it return a ne state as our data
    return state;

};
