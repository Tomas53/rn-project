import { AUTHENTICATE,LOGIN, SIGNUP, LOGOUT } from "../actions/auth";

const initState={
    token: null,
    userId:null
};

export default (state=initState, action)=>{
    switch(action.type){
            
            case LOGOUT:
                return initState;

            
                case AUTHENTICATE://LOGIN:
            return {
                token: action.token,
                userId: action.userId
            }
                
                // we return initialState which means that token is reset and user id is reset

        // case SIGNUP:
        //     return {
        //         token: action.token,
        //         userId: action.userId
        //     }
        default:
            return state;
    }//

}