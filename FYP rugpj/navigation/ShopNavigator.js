import React from 'react';
import {
    createStackNavigator,
    createDrawerNavigator,
    createAppContainer,
    DrawerItems,
    createSwitchNavigator//naudojamas loginui, nes neleidzia grizt atgal
  } from 'react-navigation';
// import { createDrawerNavigator} from '@react-navigation/drawer';
// for custumization on different plaforms
import {Platform, SafeAreaView, Button, View} from 'react-native';
import {useDispatch} from 'react-redux'

import ProductInfoScreen from '../screens/shop/ProductInfoSc'
import UserProductsScreen from "../screens/user/UserProductsScreen"
import ProductsAll from '../screens/shop/ProductsAll';
import EditProductScreen from '../screens/user/SellerProdScreen'


import * as authActions from '../store/actions/auth'

import AuthenticateView from '../screens/user/AuthenticateView';
import Colours from '../constants/Colours';
import CartScreenView from '../screens/shop/CartScreenView';
import OrdersScreen from '../screens/shop/OrderView';
import {Ionicons} from '@expo/vector-icons';
import StartView from '../screens/StartView'
import WishListView from '../screens/shop/WishListView'
import LocationMap from '../screens/shop/LocationMap'
import SupportScreen from '../screens/shop/SupportScreen'
import ChatBotScreen from '../screens/shop/ChatbotScreen'
import SignupScreen from '../screens/user/Signup'

import ImgPicker from '../screens/shop/ImgPicker'

const defaultNavOptions={
    headerStyle:{
        backgroundColor:Platform.OS==='android'? Colours.primary:''
    },
    headerTintColor: Platform.OS==='android'? 'white':Colours.primary
}


const ProductsNavigator=createStackNavigator({
    // ProductsOverview cia vardas kuri duodam siatm key
    ProductsOverview: ProductsAll,
    ProductInfo:ProductInfoScreen,
    WishList: WishListView
    

},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-cart':'ios-cart'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
});
const NavigationCart= createStackNavigator({
    Cart: CartScreenView
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-list':'ios-list'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const OrdersNavigator= createStackNavigator({
    Orders: OrdersScreen
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?ios="ios-card": md="md-card"
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const NavigationSignup= createStackNavigator({
    Signup: SignupScreen
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-list':'ios-list'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const AdminNavigator= createStackNavigator({
    UserProducts: UserProductsScreen,
    EditProduct: EditProductScreen//editproduct is identifier
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?ios="ios-brush": md="md-brush"
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})
const WishListViewNavigator= createStackNavigator({
    WishList: WishListView
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-list-box':'ios-list-box'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})
const LocationMapNavigator= createStackNavigator({
    LocationM: LocationMap
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-navigate':'ios-navigate'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})
const SupportNavigator= createStackNavigator({
    Support: SupportScreen
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?ios="ios-mail" : md="md-mail"
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const ImagePickerNavigator= createStackNavigator({
    ImgPicker: ImgPicker
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-camera':'ios-camera'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const ChatBotNavigator= createStackNavigator({
    ChatBot: ChatBotScreen
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-chatbubbles':'ios-chatbubbles'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})

const ShopNavigator= createDrawerNavigator({
    Products: ProductsNavigator,
    Cart: NavigationCart,
    Orders: OrdersNavigator,
    SellerOptions: AdminNavigator,
    // WishList: WishListViewNavigator,
    Location: LocationMapNavigator,
    Support: SupportNavigator,
    ProductPreview: ImagePickerNavigator,
    ChatBot: ChatBotNavigator
},{
    contentOptions: {
        activeTintColor: Colours.primary
    },
    contentComponent: props=>{//content components lets us to add our own content here instead of the default that drawer lets us
        const dispatch=useDispatch();
        return <View style={{flex:1, padding:25}}>
            {/* controls how our content is layed-out */}
            <SafeAreaView forceInset={{top:'always', horizontal:'never'}}> 
                {/* code below renders renders default drawer items and in order for those items to be configured correctly we pass props*/}
                <DrawerItems {...props}/>
                <Button title="logout" color={Colours.primary}
                onPress={()=>{
                    dispatch(authActions.logout());// we trigger logout Action
                    props.navigation.navigate('Auth')
                }}
                
                />
            </SafeAreaView>
        </View>
    }

})


const AuthNavigator=createStackNavigator({//in order to have an nice header
    Auth: AuthenticateView,
    Signup: SignupScreen
},{
    defaultNavigationOptions: defaultNavOptions
})
const MainNavigator=createSwitchNavigator({//wrapupinam viska i mainNavigatoriu
    Startup: StartView,
    Auth:AuthNavigator,
    Shop: ShopNavigator

})

export default createAppContainer(MainNavigator)
