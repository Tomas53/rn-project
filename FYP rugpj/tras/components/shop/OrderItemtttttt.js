import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

import CartProduct from '../../screens/shop/CartProduct';

const OrderItem=props=>{
    // const [showDetails, setShowDetails]=useState(false);//we dont show details initially

    return <View style={styles.OrderItem}> 
        <View style={styles.summary}>
            <Text style={styles.totalAmount}>£{props.amount.toFixed(2)}</Text>
            <Text style={styles.date}>{props.date}</Text>
        </View>
        {/* <Button 
        title={ showDetails ? "Hide details":"Show Details" }//changes titile according to showDetails value
        onPress={()=>{
            setShowDetails(prevState=>!prevState)//reverts the state
        }}/> */}
        
        { 
        <View style={styles.OrderItem}>
              {props.items.map(cartItem=>
              
              <CartProduct //if showDetails is true then we show this view
                key={cartItem.productId}//map requires key thus we use unique ids of products
                quantity={cartItem.quantity}//sugeneruoja CartItem detailsus
                amount={cartItem.sum}
                title={cartItem.productTitle}
                />
                )
                }
            </View>}
    </View>

}

const styles=StyleSheet.create({
    orderItem:{
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset:{width:0, height:2},
        shadowRadius: 8,
        elevation:6,
        borderRadius:11,
        backgroundColor: 'white',
        margin: 21,
        padding:10,
        alignItems: 'center',
        borderBottomWidth:12,
        borderColor:'#F5EF9D'


    },
    summary:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginBottom:16,
        borderColor:'#F5EF9D',
        borderBottomWidth:5,
        borderLeftWidth:5,
        borderRightWidth:5,
        backgroundColor:"#FEFBDA"
    },
    totalAmount:{
        
        fontSize: 15
    },
    date:{
        fontSize: 15

    }

});

export default OrderItem;