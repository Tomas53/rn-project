
import React from 'react';
import {View, Text, Image, StyleSheet,Button, TouchableOpacity } from 'react-native';

import Colours from '../../constants/Colours'

// cia bus tiks stylingas componento
const ProductComponent=props=>{
    return (
    <TouchableOpacity onPress={props.onSelect}>
    <View style={styles.product}>

        <View style={styles.imageContainer}>
        {/* gaunam duomenis per propsus */}
        <Image style={styles.image }source={{uri: props.image}}/>
        </View>
        <View style={styles.details}>
        <Text style={styles.title }>{props.title}</Text>
        {/* tofixed padaro du skaicius po kablelio */}
        <Text style={styles.price }>£{props.price.toFixed(2)}</Text>
        </View>
        <View style={styles.actions}>
            {/*  iskvieciam funkcija per propsus*/}
            {/* <Button 
            color={Colours.price}
            title="View Details" 
            onPress={props.onViewDetail}
            />
            <Button 
            color={Colours.price}
            title="to Cart" 
            onPress={props.onAddToCart}
            /> */}
            {props.children}
            {/* special prop that adds whatever we pass between opening and closing tag in our case thus we can customize product for different screens/uses */}
        </View>
    </View>
    </TouchableOpacity>
    )



};


const styles =StyleSheet.create({
    product:{
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowRadius:8,
        elevation: 5,
        borderRadius:10,
        backgroundColor: '#F5EF9D',
        height: 300,
        margin:20

    },
    image:{
        width: "100%",
        height: '100%'

    },
    title:{
        fontSize:18,
        marginVertical:4,
    },
    price:{
        fontSize:14,
        color: '#888'
    },
    actions:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '22%',
        paddingHorizontal:20


    },
    details:{
        alignItems:"center",
        height: '18%',
        padding: 10
    },
    imageContainer:{
        width: "100%",
        height: '60%',
        // makes corner rounded
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        // child cant overlap
        overflow: 'hidden'

    }
});


export default ProductComponent;