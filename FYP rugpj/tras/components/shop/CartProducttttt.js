

import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from "react-native";
import {Ionicons} from "@expo/vector-icons"

const CartProduct=props=>{
    return( <View style={styles.cartProduct}>
        <Text style={styles.ProductData}>
            <Text styles={styles.quantity}>{props.quantity}</Text>
            <Text styles={styles.mainText}> {props.title}</Text>
            
        </Text>
        <View style={styles.productData}>
            <Text styles={styles.mainText}>£{props.amount.toFixed(2)}</Text>
         
            <TouchableOpacity onPress={props.onRemove} style={styles.deleteButton}>
                <Ionicons 
                  name="md-close"
                  size={21}
                  color="pink"
                />
            </TouchableOpacity>
        </View>

    </View>
    )
};

const styles=StyleSheet.create({
    cartProduct:{
        padding: 10,
        backgroundColor: '#FEFBDA',
        flexDirection: 'row',
        justifyContent: "space-between",
        marginHorizontal: 21
    },
    productData: {
        flexDirection: "row",
        alignItems: "center"
    },
    quantity:{
        color: 'grey',
        fontSize: 15
        
    },
    mainText: {
        fontSize: 15
    },
    
    deleteButton: {
        marginLeft: 20
    }


});

export default CartProduct;