import React, {useReducer, useEffect} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';


const INPUT_CHANGE='INPUT_CHANGE';
const INPUT_BLUR='INPUT_BLUR';

const inputReducer=(state, action)=>{
    switch(action.type){//checks action type
        case INPUT_CHANGE:
            return{
                ...state,
                value: action.value,//updating value
                isValid: action.isValid//checks if specific input is valid and not entire form
            }
        case INPUT_BLUR:
            return{
                ...state,
                touched:true
            }
        
            default:
                return state;
    }
};

const Input=props=>{
    //returns us inputState snapshot and dispatch function
    const [inputState, dispatch]=useReducer(inputReducer, {
        value: props.initialValue ? props.initialValue: '',//check if initil value is set basically if we in edit mode
        isValid: props.initiallyValid,//checks if props are valid
        touched: false//checks if user typed anything into form
    })

    const {onInputChange, id}=props;
    useEffect(()=>{
        if(inputState.touched){//we only send this info if touched is true
            onInputChange(id,inputState.value, inputState.isValid);//parent function if triggered receives these arguments

        }
    }, [inputState, onInputChange, id]);//useEffect runs when inputState, onInputChange, id changes
    const textChangeHandler=text=>{
        //check if input valid or not

        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // email check regex
        let isValid = true;
        if (props.required && text.trim().length === 0) {
        isValid = false;
        }
        if (props.email && !emailRegex.test(text.toLowerCase())) {
        isValid = false;
        }
        //checks if number is too big or too small
        if (props.min != null && +text < props.min) {
        isValid = false;
        }
        if (props.max != null && +text > props.max) {
        isValid = false;
        }
        if (props.minLength != null && text.length < props.minLength) {
        isValid = false;
        }
        dispatch({type: INPUT_CHANGE, value:text, isValid:isValid})//disatches action

    }


    const lostFocusHandler=()=>{//we update touch when we loose focus(when user is done typing in certain input area)
        dispatch({type:INPUT_BLUR})

    }
    return <View style={styles.formControl}>
                <Text style={styles.label}>{props.label}</Text>{/* here text is dynamic because of props.label */}
                {/* validation for form happens here and not in the parent component */}
                <TextInput 
                {...props}
                style={styles.input} 
                value={inputState.value} //we are using inputState as our internal state
                onChangeText={textChangeHandler}//
                onBlur={lostFocusHandler}//activates when user types in this specific input
                />
                {!inputState.isValid&& inputState.touched &&(
                    <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{props.errorText}</Text>{/*we have dynamic eroor message*/}
                </View>
                )}
            </View>
            
            {/* litle error message */}

    

}

const styles=StyleSheet.create({
    formControl:{
        width:'100%'

    },
    label: {
        marginVertical: 8
    },
    input:{
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1

    },
    errorContainer: {
        marginVertical: 5
    },
    errorText:{
        color: 'red',
        fontSize: 12
    }
})
export default Input;